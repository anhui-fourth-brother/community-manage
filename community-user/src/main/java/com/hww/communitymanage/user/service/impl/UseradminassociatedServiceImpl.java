package com.hww.communitymanage.user.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hww.common.utils.PageUtils;
import com.hww.common.utils.Query;

import com.hww.communitymanage.user.dao.UseradminassociatedDao;
import com.hww.communitymanage.user.entity.UseradminassociatedEntity;
import com.hww.communitymanage.user.service.UseradminassociatedService;


@Service("useradminassociatedService")
public class UseradminassociatedServiceImpl extends ServiceImpl<UseradminassociatedDao, UseradminassociatedEntity> implements UseradminassociatedService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<UseradminassociatedEntity> page = this.page(
                new Query<UseradminassociatedEntity>().getPage(params),
                new QueryWrapper<UseradminassociatedEntity>()
        );

        return new PageUtils(page);
    }

}