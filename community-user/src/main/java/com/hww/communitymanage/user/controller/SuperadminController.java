package com.hww.communitymanage.user.controller;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hww.common.utils.*;
import com.hww.communitymanage.user.config.RedisUtil;
import com.hww.communitymanage.user.entity.StudentsEntity;
import com.hww.communitymanage.user.service.StudentsService;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.hww.communitymanage.user.entity.SuperadminEntity;
import com.hww.communitymanage.user.service.SuperadminService;


/**
 * 超级管理员表
 *
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 16:42:33
 */
@RestController
@RequestMapping("user/superadmin")
public class SuperadminController {
    @Autowired
    private SuperadminService superadminService;
    @Autowired
    private StudentsService studentsService;
    //导入redis
    @Autowired
    RedisUtil redisUtil;

    /**
     * 列表
     */
//    @RequestMapping("/list")
//    //@RequiresPermissions("user:superadmin:list")
//    public R list(@RequestParam Map<String, Object> params){
//        PageUtils page = superadminService.queryPage(params);
//
//        return R.ok().put("page", page);
//    }


    /**
     * 根据主键获取用户信息
     */

    public SuperadminEntity info(String recId) {
        SuperadminEntity students = superadminService.getById(recId);
        return students;
    }
    @RequestMapping("/updateSuperAdmin")
    //@RequiresPermissions("user:students:update")
    public R update(@RequestParam Map<String, String> params,@RequestHeader("token") String token) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN+token))){
            return R.error(Constant.TOKEN,"登录信息失效，请重新登陆");
        }
        String oldPws = params.get("oldPws");
        String newPws= new Md5().EncoderByMd5(params.get("newPws"));
        String recId = params.get("recId");
        SuperadminEntity sup = info(recId);
        Md5 md5 = new Md5();
        //校验密码是否正确
        if (md5.checkpassword(oldPws, sup.getAdminPws())) {
            sup.setAdminPws(newPws);
            superadminService.updateById(sup);
            return R.ok();
        } else {
            return R.error(-9999, "请输入正确的密码");
        }

//
//        studentsService.updateById(students);


    }
//    /**
//     * 保存
//     */
//    @RequestMapping("/save")
//    //@RequiresPermissions("user:superadmin:save")
//    public R save(@RequestBody SuperadminEntity superadmin){
//		superadminService.save(superadmin);
//
//        return R.ok();
//    }

//    /**
//     * 修改
//     */
//    @RequestMapping("/updateSuperAdmin")
//    //@RequiresPermissions("user:superadmin:update")
//    public R update(@RequestBody SuperadminEntity superadmin){
//        superadminService.updateById(superadmin);
//        return R.ok();
//    }

    /**
     * 超级管理员登录
     */
    @RequestMapping("admin/login")

    //@RequiresPermissions("user:students:delete")
    public R login(@RequestParam Map<String, String> params) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String pws = params.get("adminPassword");
        String userId = params.get("userId");
        //匹配加密后的密码
        String redisLoginCount= Constant.LOGIN_COUNT+userId;
        //同一账号 5分钟内登录十次锁5分钟
        if (redisUtil.hasKey(redisLoginCount)){
            redisUtil.incr(redisLoginCount,1);
            redisUtil.expire(redisLoginCount,60*5);
        }else {
            redisUtil.set(redisLoginCount, 0, 60 * 5);
        }
        if ((int)redisUtil.get(redisLoginCount)>9){
            redisUtil.expire(redisLoginCount,60*5);
            return R.error(Constant.ERROR, "错误次数过多，请5分钟后重新尝试");
        }
        pws = new Md5().EncoderByMd5(pws);
        QueryWrapper<SuperadminEntity> querwrapper = new QueryWrapper<>();
        querwrapper.eq("ADMIN_USER",userId).eq("ADMIN_PWS",pws);
        SuperadminEntity superadminEntity=superadminService.getOne(querwrapper);
        if (superadminEntity == null) {
            QueryWrapper<StudentsEntity> querwrapper1 = new QueryWrapper<>();
            querwrapper1.eq("USER_ID", userId).eq("STU_PASSWORD", pws).eq("POSITION", 1);
            StudentsEntity studentsEntity = studentsService.getOne(querwrapper1);
            if (studentsEntity==null){
                return R.error(Constant.ERROR, "用户名或密码错误");
            }
            redisUtil.del(redisLoginCount);
            //要转存jwt的信息
            Map<String, String> map = new HashMap<>();
            map.put("userId", studentsEntity.getUserId());
            map.put("stuName", studentsEntity.getStuName());
            String token = JwtUtils.createJsonWebToken(map);
            redisUtil.set(Constant.LOGIN_TOKEN + token, token, 60 * 60 * 3);
            return R.ok().put("token",token).put("data",studentsEntity);
        }
        redisUtil.del(redisLoginCount);
        Map<String,String> map =new HashMap<>();
        map.put("userId",superadminEntity.getAdminUser());
        map.put("stuName",superadminEntity.getAdminPws());
        String token = JwtUtils.createJsonWebToken(map);
        redisUtil.set(Constant.LOGIN_TOKEN + token, token, 60 * 60 * 3);
        return R.ok().put("token",token).put("data",superadminEntity);
    }

}
