package com.hww.communitymanage.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hww.common.utils.PageUtils;
import com.hww.communitymanage.user.entity.UseradminassociatedEntity;

import java.util.Map;

/**
 * 学生管理员关联表
 *
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 16:42:33
 */
public interface UseradminassociatedService extends IService<UseradminassociatedEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

