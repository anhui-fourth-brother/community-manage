package com.hww.communitymanage.user.dao;

import com.hww.communitymanage.user.entity.UseradminassociatedEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 学生管理员关联表
 * 
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 16:42:33
 */
@Mapper
public interface UseradminassociatedDao extends BaseMapper<UseradminassociatedEntity> {
	
}
