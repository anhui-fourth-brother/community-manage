package com.hww.communitymanage.user.feign;


import com.hww.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * 这是一个声明式的远程调用
 */
@FeignClient("community-activity")
public interface UserFeignService {
    @RequestMapping("/activity/activity/list")
    public R membercoupons();
}
