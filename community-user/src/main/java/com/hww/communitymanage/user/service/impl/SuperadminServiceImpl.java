package com.hww.communitymanage.user.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hww.common.utils.PageUtils;
import com.hww.common.utils.Query;

import com.hww.communitymanage.user.dao.SuperadminDao;
import com.hww.communitymanage.user.entity.SuperadminEntity;
import com.hww.communitymanage.user.service.SuperadminService;


@Service("superadminService")
public class SuperadminServiceImpl extends ServiceImpl<SuperadminDao, SuperadminEntity> implements SuperadminService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SuperadminEntity> page = this.page(
                new Query<SuperadminEntity>().getPage(params),
                new QueryWrapper<SuperadminEntity>()
        );

        return new PageUtils(page);
    }

}