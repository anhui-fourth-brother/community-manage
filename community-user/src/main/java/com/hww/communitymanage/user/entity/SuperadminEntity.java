package com.hww.communitymanage.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 超级管理员表
 * 
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 16:42:33
 */
@Data
@TableName("user_superadmin")
public class SuperadminEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.ASSIGN_UUID)   //使用UUID策略来生成主键
	private String recId;
	/**
	 * 账号
	 */
	private String adminUser;
    /**
     * 密码
     */
    private String adminPws;
    /**
     * 拓展字段1
     */
    @TableField(select = false)   //查询不返回此字段
    private String expand1;
    /**
     * 拓展字段2
     */
    @TableField(select = false)   //查询不返回此字段
    private String expand2;

}
