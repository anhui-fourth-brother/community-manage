package com.hww.communitymanage.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 学生管理员关联表
 * 
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 16:42:33
 */
@Data
@TableName("user_useradminassociated")
public class UseradminassociatedEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.ASSIGN_UUID)   //使用UUID策略来生成主键
	private String recId;
	/**
	 * 学号
	 */
	private String sno;
	/**
	 * 操作时间
	 */
	private String dealTime;
    /**
     * 职位0学生1社团管理员
     */
    private Integer position;
    /**
     * 拓展字段1
     */
    @TableField(select = false)   //查询不返回此字段
    private String expand1;
    /**
     * 拓展字段2
     */
    @TableField(select = false)   //查询不返回此字段
    private String expand2;
    /**
     * 学生姓名
     */
    private String userName;

}
