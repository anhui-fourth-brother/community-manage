package com.hww.communitymanage.association.service.impl;

import com.hww.communitymanage.association.entity.CommunityEntity;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hww.common.utils.PageUtils;
import com.hww.common.utils.Query;

import com.hww.communitymanage.association.dao.CommunityuserDao;
import com.hww.communitymanage.association.entity.CommunityuserEntity;
import com.hww.communitymanage.association.service.CommunityuserService;
import org.springframework.util.StringUtils;


@Service("communityuserService")
public class CommunityuserServiceImpl extends ServiceImpl<CommunityuserDao, CommunityuserEntity> implements CommunityuserService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<CommunityuserEntity> querwrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(params.get("userId"))){
            querwrapper.eq("USER_ID",params.get("userId"));
        }
        if (!StringUtils.isEmpty(params.get("communityId"))){
            querwrapper.eq("COMMUNITY_ID",params.get("communityId"));
        }
        if (!StringUtils.isEmpty(params.get("position"))){
            querwrapper.eq("POSITION",params.get("position"));
        }
        IPage<CommunityuserEntity> page = this.page(
                new Query<CommunityuserEntity>().getPage(params),
                querwrapper
        );

        return new PageUtils(page);
    }

}