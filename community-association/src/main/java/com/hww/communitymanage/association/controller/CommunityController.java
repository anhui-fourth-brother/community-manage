package com.hww.communitymanage.association.controller;

import java.util.*;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hww.common.utils.*;
import com.hww.communitymanage.association.entity.CommunityuserEntity;
import com.hww.communitymanage.association.entity.CommunityuserstreamEntity;
import com.hww.communitymanage.association.service.CommunityuserService;
import com.hww.communitymanage.association.service.CommunityuserstreamService;
import com.hww.communitymanage.association.utils.RedisUtil;
import com.hww.communitymanage.association.entity.CommunitycheckEntity;
import com.hww.communitymanage.association.feign.AssociationFeignService;
import com.hww.communitymanage.association.service.CommunitycheckService;
import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.api.RReadWriteLock;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.hww.communitymanage.association.entity.CommunityEntity;
import com.hww.communitymanage.association.service.CommunityService;


/**
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 15:58:09
 */
@RestController
@RequestMapping("association/community")
public class CommunityController {
    //导入redis
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private CommunityService communityService;
    @Autowired(required = false)
    Redisson redisson;
    @Autowired
    CommunitycheckService communitycheckService;
    @Autowired
    private CommunityuserService communityuserService;
    @Autowired
    private CommunityuserstreamService communityuserstreamService;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * rabbit发送消息
     */
    private void sendMsg(Object msg) {
        String routingKey = "baseMsg";
        String content = JSON.toJSONString(msg);
        rabbitTemplate.convertAndSend(routingKey, content);
    }

    /**
     * 查询社团列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("association:community:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = communityService.queryPage(params);
        List<CommunityEntity> list = (List<CommunityEntity>) page.getList();
        for (int i = 0; i < list.size(); i++) {
            Integer activityNm = 0;
            if (redisUtil.hasKey(Constant.COMMUNITY_ACT_NUM + list.get(i).getCommunityId())) {
                activityNm = (Integer) redisUtil.get(Constant.COMMUNITY_ACT_NUM + list.get(i).getCommunityId());
            }
            list.get(i).setExpand1(activityNm == null ? 0 : activityNm);
        }
        return R.ok().put("data", page).put("stuAllNm", redisUtil.get(Constant.STUDENT_NUM)).put("activityNm", redisUtil.get(Constant.ACTIVITY_NUM));
    }

    /**
     * 申请创建社团
     */
    @RequestMapping("/save")
    //@RequiresPermissions("association:community:save")
    public R save(@RequestBody CommunityEntity community, @RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN + token))) {
            return R.error(Constant.TOKEN, "登录信息失效，请重新登陆");
        }
        QueryWrapper<CommunitycheckEntity> querwrapper = new QueryWrapper<>();
        querwrapper.eq("USER_APPLY_ID", community.getCommunityCreater()).ne("SUPERADMIN_STATE", 1).ne("NOW_STATE", 3);
        CommunitycheckEntity communitycheckServiceOne = communitycheckService.getOne(querwrapper);
        if (communitycheckServiceOne != null) {
            return R.error(Constant.ERROR, "该社团请求正在处理中，请勿重复提交");
        }

        String communityId = IdUtils.getIncreaseIdByNanoTime();
        community.setCommunityId(communityId);
        communityService.save(community);
        CommunitycheckEntity communitycheckEntity = CommunityEntityToCommunitycheckEntity(community);
        communitycheckService.save(communitycheckEntity);
        //发送消息
        MessageUtli messageUtli = new MessageUtli(community.getCommunityId(), null, null, community.getCommunityName(), null, DateUtils.getLocalDateTimeStr(),
                community.getCommunityCreater(), community.getCommunityCreaterName(), "admin", "admin", null, 7);
        sendMsg(messageUtli);
        return R.ok();
    }

    /**
     * 将社团对象转为审批表对象
     *
     * @param communityEntity
     * @return CommunitycheckEntity
     */
    private CommunitycheckEntity CommunityEntityToCommunitycheckEntity(CommunityEntity communityEntity) {
        CommunitycheckEntity communitycheckEntity = new CommunitycheckEntity();
        communitycheckEntity.setDealTime(DateUtils.getLocalDateTimeStr());
        communitycheckEntity.setUserApplyId(String.valueOf(communityEntity.getCommunityCreater()));
        communitycheckEntity.setApplyView(communityEntity.getApplyAdvice());
        communitycheckEntity.setCommunityId(communityEntity.getCommunityId());
        communitycheckEntity.setCommunityName(communityEntity.getCommunityName());
        communitycheckEntity.setUserApplyName(communityEntity.getCommunityCreaterName());
        communitycheckEntity.setApplyType(0);
        communitycheckEntity.setNowState(0);
        return communitycheckEntity;
    }

    /**
     * 修改社团信息
     */
    @RequestMapping("/update")
    //@RequiresPermissions("association:community:update")
    public R update(@RequestBody CommunityEntity community, @RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN + token))) {
            return R.error(Constant.TOKEN, "登录信息失效，请重新登陆");
        }
        communityService.updateById(community);
        //修改社团信息后  删除redis
        CommunityEntity communityEntity = communityService.getById(community.getRecId());
        String key = Constant.COMMUNITY_ASSOCIATION + communityEntity.getCommunityId();
        redisUtil.del(key);
        return R.ok();
    }

    /**
     * 删除社团
     */
    @RequestMapping("/delete/{recId}")
    //@RequiresPermissions("association:community:delete")
    public R delete(@PathVariable("recId") String recId, @RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN + token))) {
            return R.error(Constant.TOKEN, "登录信息失效，请重新登陆");
        }
        CommunityEntity communityEntity = communityService.getById(recId);
        if (redisUtil.hasKey(Constant.COMMUNITY_ACTIVITY_NUM + communityEntity.getCommunityId()) && (int) redisUtil.get(Constant.COMMUNITY_ACTIVITY_NUM + communityEntity.getCommunityId()) > 0) {
            return R.error(Constant.ERROR, "该社团有活动未处理完成，请先处理其活动");
        }
        QueryWrapper<CommunityuserEntity> querwrapper = new QueryWrapper<>();
        querwrapper.eq("COMMUNITY_ID", communityEntity.getCommunityId()).eq("USER_STATE", 1);
        List<CommunityuserEntity> communityuserEntities = communityuserService.list(querwrapper);
        List<CommunityuserstreamEntity> communityuserstreamEntities = new ArrayList<>();
        communityuserService.remove(querwrapper);
        for (int i = 0; i < communityuserEntities.size(); i++) {
            CommunityuserstreamEntity communityuserstreamEntity = communityUserToCommunityuserstreamEntity(communityuserEntities.get(i));
            if (Objects.equals(communityuserEntities.get(i).getPosition(), 1)) {
                communityuserstreamEntity.setPosition(1);
            }
            communityuserstreamEntities.add(communityuserstreamEntity);
        }
        //清空该社团的所有人员记录
        communityuserService.remove(querwrapper);
        //保存该社团的退社信息
        communityuserstreamService.saveBatch(communityuserstreamEntities);
        redisUtil.del(Constant.COMMUNITY_POSITION_USERID + communityEntity.getCommunityPresident());
        //redis中删除
        communityService.removeById(recId);
        redisUtil.del(Constant.COMMUNITY_ASSOCIATION + communityEntity.getCommunityId());
        //发送消息通知给社团所有人员
        List list = new ArrayList();
        for (int i = 0; i < communityuserEntities.size(); i++) {
            if (communityuserEntities.get(i).getPosition() != 1) {
                list.add(communityuserEntities.get(i).getUserId());
            }
        }
        MessageUtli messageUtli1 = new MessageUtli(communityEntity.getCommunityId(), "您所在的" + communityEntity.getCommunityName() + "已解散", "社团解散通知", communityEntity.getCommunityName(), null, DateUtils.getLocalDateTimeStr(),
                "superAdmin", "superAdmin", ConUtils.listToString(list), null, null, 0);
        sendMsg(messageUtli1);
        //给社长发送解散通知
        MessageUtli messageUtli2 = new MessageUtli(communityEntity.getCommunityId(), "您所在的" + communityEntity.getCommunityName() + "已解散", "社团解散通知", communityEntity.getCommunityName(), null, DateUtils.getLocalDateTimeStr(),
                "superAdmin", "superAdmin", communityEntity.getCommunityPresident(), null, null, 0);
        sendMsg(messageUtli2);
        return R.ok();
    }

    //社团人员转换为社团人员流动对象
    private CommunityuserstreamEntity communityUserToCommunityuserstreamEntity(CommunityuserEntity communityuserEntity) {
        CommunityuserstreamEntity communityuserstreamEntity = new CommunityuserstreamEntity();
        communityuserstreamEntity.setCommunityId(communityuserEntity.getCommunityId());
        communityuserstreamEntity.setUserId(communityuserEntity.getUserId());
        communityuserstreamEntity.setApplyState(1);
        communityuserstreamEntity.setJoinTime(DateUtils.getLocalDateTimeStr());
        communityuserstreamEntity.setPosition(0);
        communityuserstreamEntity.setOpState(3);
        communityuserstreamEntity.setCommunityName(communityuserEntity.getCommunityName());
        return communityuserstreamEntity;
    }

    /**
     * 社团热度统计
     */
    @RequestMapping("/hoteNum/{communityId}")
    public R updateHoteNum(@PathVariable("communityId") String communityId) {
        String redisKey = Constant.COMMUNITY_ASSOCIATION_HOT + communityId;
        long num = redisUtil.incr(redisKey, 1);
        if (num % 5 == 0) {
            CommunityEntity communityEntity = getCommunityInfo(communityId);
            communityEntity.setHotNum((int) num);
            communityService.updateById(communityEntity);
            redisUtil.del(Constant.COMMUNITY_ASSOCIATION + communityId);
            getCommunityInfo(communityId);
        }
        return R.ok().put("communityNum", num);
    }
//    /**
//     *测试写锁
//     */
//    @RequestMapping("/inf/{communityId}")
//    public R getCommunityByCommunityId1(@PathVariable("communityId") String communityId) throws InterruptedException {
//        //加的为正常的锁非读写锁
////        RLock lock = redissonClient.getLock("communityInfoLock");
//        RReadWriteLock readLock = redisson.getReadWriteLock("communityInfoLock");
//        //设置 加锁时常   不设置默认为 看门狗  自动监控线程  30s解锁    设置时间要大于程序运行时间，不然出错
//        RLock rLock = readLock.writeLock();
//        rLock.lock();
//        System.out.println("拿到写锁成功，开始干活了" + Thread.currentThread().getId());
//        //判断缓存是否有数据 有的话直接返回，没有的话去查库
//        Thread.sleep(30000);
//        stringRedisTemplate.opsForValue().set(Constant.COMMUNITY_ASSOCIATION + communityId, "我是写锁", 60, TimeUnit.SECONDS);
//        rLock.unlock();
//        System.out.println("我释放写锁啦");
//        return R.ok();
//    }

    /**
     * 根据社团ID查询此社团相关消息
     */
    @RequestMapping("/info/communityInfo/{communityId}")
    public R getCommunityByCommunityId(@PathVariable("communityId") String communityId) {
        return R.ok().put("date", getCommunityInfo(communityId)).put("activityNum", redisUtil.get(Constant.COMMUNITY_ACT_NUM + communityId));
    }

    /**
     * 根据社团ID拿对象
     *
     * @param communityId
     * @return
     */
    public CommunityEntity getCommunityInfo(String communityId) {
        //加的为正常的锁非读写锁
//        RLock lock = redissonClient.getLock("communityInfoLock");
        //添加读锁  防止缓存出现脏数据 防止缓存穿透
        RReadWriteLock readLock = redisson.getReadWriteLock("communityInfoLock");
        RLock rLock = readLock.readLock();
        //设置 加锁时常   不设置默认为 看门狗  自动监控线程  30s解锁    设置时间要大于程序运行时间，不然出错
        rLock.lock();
        String key = Constant.COMMUNITY_ASSOCIATION + communityId;
        //判断缓存是否有数据 有的话直接返回，没有的话去查库
        if (redisUtil.hasKey(key)) {
            String communityRedisInfo = String.valueOf(redisUtil.get(key));
            CommunityEntity communityEntity = JSON.parseObject(communityRedisInfo, CommunityEntity.class);
            rLock.unlock();
            return communityEntity;
        } else {
            QueryWrapper<CommunityEntity> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("COMMUNITY_ID", communityId);
            CommunityEntity communityEntity = communityService.getOne(queryWrapper);
            if (org.springframework.util.StringUtils.isEmpty(communityEntity)) {
                //防止缓存穿透
                redisUtil.set(key, "null", 2);
                rLock.unlock();
                return null;
            }
            String s = JSON.toJSONString(communityEntity);
            redisUtil.set(key, s, 60 * 60 * 24);
            rLock.unlock();
            return communityEntity;
        }

    }


}
