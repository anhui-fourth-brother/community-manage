package com.hww.communitymanage.association.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 15:58:09
 */
@Data
@TableName("ass_communityuserstream")
public class CommunityuserstreamEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.ASSIGN_UUID)   //使用UUID策略来生成主键
	private String recId;
	/**
	 * 社团ID
	 */
	private String communityId;
	/**
	 * 人员ID
	 */
	private String userId;
	/**
	 * 入社时间
	 */
	private String joinTime;
	/**
	 * 申请入社时间
	 */
	private String applyTime;
	/**
	 * 申请入社状态 0待处理 1同意 2拒绝
	 */
	private Integer applyState;
	/**
	 * 入社理由
	 */
	private String applyReason;
	/**
	 * 职位0社员1社长
	 */
	private Integer position;
	/**
	 * 操作状态0离社1入社
	 */
	private Integer opState;
	/**
	 * 审批人
	 */
	private String userCheck;
	/**
	 * 最后修改时间
	 */
	private String updateTime;
	/**
	 * 社团名称
	 */
	private String communityName;
	/**
	 * 申请姓名
	 */
	private String userName;

}
