package com.hww.communitymanage.association.service.impl;

import com.hww.communitymanage.association.entity.CommunityEntity;
import org.springframework.stereotype.Service;

import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hww.common.utils.PageUtils;
import com.hww.common.utils.Query;

import com.hww.communitymanage.association.dao.CommunitycheckDao;
import com.hww.communitymanage.association.entity.CommunitycheckEntity;
import com.hww.communitymanage.association.service.CommunitycheckService;
import org.springframework.util.StringUtils;


@Service("communitycheckService")
public class CommunitycheckServiceImpl extends ServiceImpl<CommunitycheckDao, CommunitycheckEntity> implements CommunitycheckService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<CommunitycheckEntity> querwrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(params.get("nowState"))) {
            querwrapper.eq("NOW_STATE", params.get("nowState"));
        }
        if (!StringUtils.isEmpty(params.get("adminState"))) {
            querwrapper.eq("ADMIN_STATE", params.get("adminState"));
        }
        if (!StringUtils.isEmpty(params.get("applyType"))) {
            querwrapper.eq("APPLY_TYPE", params.get("applyType"));
        }
        if (!StringUtils.isEmpty(params.get("userId"))) {
            querwrapper.eq("USER_ID", params.get("userId"));
        }
        if (!StringUtils.isEmpty(params.get("userApplyId"))) {
            querwrapper.eq("USER_APPLY_ID", params.get("userApplyId"));
        }
        if (!StringUtils.isEmpty(params.get("superadminState"))) {
            querwrapper.eq("SUPERADMIN_STATE", params.get("superadminState"));
        }
        IPage<CommunitycheckEntity> page = this.page(
                new Query<CommunitycheckEntity>().getPage(params),
                querwrapper
        );

        return new PageUtils(page);
    }

}