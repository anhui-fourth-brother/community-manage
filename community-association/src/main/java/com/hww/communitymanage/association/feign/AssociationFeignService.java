package com.hww.communitymanage.association.feign;


import com.hww.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;


/**
 * 这是一个声明式的远程调用
 */
@FeignClient("community-activity")
public interface AssociationFeignService {
    @RequestMapping("/activity/activity/list")
    public R getActivityInfoByCommunityId(Map map);
}
