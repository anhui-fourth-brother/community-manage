package com.hww.communitymanage.association.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hww.common.utils.PageUtils;
import com.hww.communitymanage.association.entity.CommunityuserEntity;

import java.util.Map;

/**
 * 
 *
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 15:58:09
 */
public interface CommunityuserService extends IService<CommunityuserEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

