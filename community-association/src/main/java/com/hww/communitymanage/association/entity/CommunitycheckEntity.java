package com.hww.communitymanage.association.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 15:58:09
 */
@Data
@TableName("ass_communitycheck")
public class CommunitycheckEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.ASSIGN_UUID)   //使用UUID策略来生成主键
	private String recId;
	/**
	 * 审批状态0建社1换届
	 */
	private Integer applyType;
	/**
	 * 申请意见
	 */
	private String applyView;
	/**
	 * 申请人
	 */
	private String userApplyId;
	/**
	 * 申请人
	 */
	private String newPosition;
	/**
	 * 申请人
	 */
	private String newPositionName;
	/**
	 * 社团ID
	 */
	private String communityId;
	/**
	 * 审批人ID
	 */
	private String userId;
	/**
	 * 社团申请附件
	 */
	private Integer nowState;
	/**
	 * 审批人姓名
	 */
	private String userName;
	/**
	 * 审批时间
	 */
	private String dealTime;
	/**
	 * 审批意见
	 */
	private String adminAdvice;
	/**
	 * 审批意见
	 */
	private String superadminAdvice;
	/**
	 * 超级管理员处理时间
	 */
	private String superadminTime;
	/**
	 * 管理员处理时间
	 */
	private String adminTime;
	/**
	 * 最后更新时间
	 */
	private String updateTime;
	/**
	 * 申请人姓名
	 */
	private String userApplyName;
	/**
	 * 管理员处理状态
	 */
	private Integer adminState;
	/**
	 * 超级管理员处理状态
	 */
	private Integer superadminState;
	/**
	 * 社团名称
	 */
	private String communityName;
	/**
	 * 乐观锁字段
	 */
	@Version
	private Integer version;

}
