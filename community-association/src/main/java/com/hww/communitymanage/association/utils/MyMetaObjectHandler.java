package com.hww.communitymanage.association.utils;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.hww.common.utils.IdUtils;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

@Component
public class MyMetaObjectHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        //插入操作时 若此属性没有数据 执行赋值  有数据会覆盖此赋值
//        this.setFieldValByName("communityId", IdUtils.getIncreaseIdByNanoTime(), metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {

    }
}
