package com.hww.communitymanage.association.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hww.common.utils.*;
import com.hww.communitymanage.association.utils.RedisUtil;
import com.hww.communitymanage.association.entity.CommunityEntity;
import com.hww.communitymanage.association.entity.CommunitycheckEntity;
import com.hww.communitymanage.association.entity.CommunityuserEntity;
import com.hww.communitymanage.association.entity.CommunityuserstreamEntity;
import com.hww.communitymanage.association.service.CommunityService;
import com.hww.communitymanage.association.service.CommunitycheckService;
import com.hww.communitymanage.association.service.CommunityuserService;
import com.hww.communitymanage.association.service.CommunityuserstreamService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;


/**
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 15:58:09
 */
@RestController
@RequestMapping("association/communitycheck")
public class CommunitycheckController {
    @Autowired
    private CommunitycheckService communitycheckService;
    @Autowired
    private CommunityuserService communityuserService;
    @Autowired
    private CommunityuserstreamService communityuserstreamService;
    @Autowired
    RedisUtil redisUtil;
    @Autowired
    private CommunityController communityController;
    @Autowired
    private CommunityService communityService;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private CommunityuserController communityuserController;


    /**
     * 按条件查询审批列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("association:communitycheck:list")
    public R list(@RequestParam Map<String, Object> params, @RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN + token))) {
            return R.error(Constant.TOKEN, "登录信息失效，请重新登陆");
        }
        PageUtils page = communitycheckService.queryPage(params);
        return R.ok().put("page", page);
    }


//    /**
//     * 信息
//     */
//    @RequestMapping("/info/{recId}")
//    //@RequiresPermissions("association:communitycheck:info")
//    public R info(@PathVariable("recId") String recId){
//		CommunitycheckEntity communitycheck = communitycheckService.getById(recId);
//
//        return R.ok().put("communitycheck", communitycheck);
//    }

    /**
     * rabbit发送消息
     */
    private void sendMsg(Object msg) {
        String routingKey = "baseMsg";
        String content = JSON.toJSONString(msg);
        rabbitTemplate.convertAndSend(routingKey, content);
    }

    /**
     * 新增社团审批接口
     */
    @RequestMapping("/saveCommunityCheck")
    //@RequiresPermissions("association:communitycheck:save")
    public R save(@RequestBody CommunitycheckEntity communitycheck, @RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN + token))) {
            return R.error(Constant.TOKEN, "登录信息失效，请重新登陆");
        }
        QueryWrapper<CommunitycheckEntity> querwrapper = new QueryWrapper<>();
        querwrapper.eq("COMMUNITY_ID", communitycheck.getCommunityId()).eq("NOW_STATE", 0).or().eq("NOW_STATE", 1);
        CommunitycheckEntity communitycheckEntity = communitycheckService.getOne(querwrapper);
        if (communitycheckEntity != null) {
            return R.error(Constant.ERROR, "该社团有申请正在处理中，请勿重复提交");
        }
        if (communitycheck.getApplyType() == 1 && redisUtil.hasKey(Constant.COMMUNITY_POSITION_USERID + communitycheck.getNewPosition())) {
            return R.error(Constant.ERROR, "该人已是社长，请换个人换届");
        }
        //给管理员发送消息
        MessageUtli messageUtli = new MessageUtli(communitycheck.getCommunityId(), null, null, communitycheck.getCommunityName(), null, DateUtils.getLocalDateTimeStr(),
                communitycheck.getUserApplyId(), communitycheck.getUserApplyName(), "admin", null, null, 7);
        sendMsg(messageUtli);
        communitycheckService.save(communitycheck);
        return R.ok();
    }

    /**
     * 转换审批表对象为社团人员流动对象
     *
     * @param communitycheck
     * @return
     */
    private CommunityuserstreamEntity communityuserstreamToCommunityuser(CommunitycheckEntity communitycheck) {
        CommunityuserstreamEntity communityuserstreamEntity = new CommunityuserstreamEntity();
        communityuserstreamEntity.setCommunityId(communitycheck.getCommunityId());
        communityuserstreamEntity.setUserCheck(communitycheck.getUserId());
        communityuserstreamEntity.setUserId(communitycheck.getUserApplyId());
        communityuserstreamEntity.setJoinTime(communitycheck.getSuperadminTime());
        communityuserstreamEntity.setApplyReason(communitycheck.getApplyView());
        communityuserstreamEntity.setPosition(1);
        communityuserstreamEntity.setOpState(1);
        communityuserstreamEntity.setJoinTime(DateUtils.getLocalDateTimeStr());
        communityuserstreamEntity.setCommunityName(communitycheck.getCommunityName());
        communityuserstreamEntity.setUserName(communitycheck.getUserApplyName());
        communityuserstreamEntity.setApplyTime(communitycheck.getDealTime());
        return communityuserstreamEntity;
    }

    /**
     * 转换审批表对象为社团成员对象
     *
     * @param communitycheck
     * @return
     */
    private CommunityuserEntity communitycheckToCommunityuserEntity(CommunitycheckEntity communitycheck) {
        CommunityuserEntity communityuserEntity = new CommunityuserEntity();
        communityuserEntity.setCommunityId(communitycheck.getCommunityId());
        communityuserEntity.setUserId(communitycheck.getUserApplyId());
        communityuserEntity.setJoinTime(communitycheck.getDealTime());
        communityuserEntity.setPosition(1);
        communityuserEntity.setAdminUser(communitycheck.getUserId());
        communityuserEntity.setCommunityName(communitycheck.getCommunityName());
        return communityuserEntity;
    }

    /**
     * 审核社团信息
     */
    @RequestMapping("/update")
    //@RequiresPermissions("association:communitycheck:update")
    public R update(@RequestBody CommunitycheckEntity communitycheck, @RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN + token))) {
            return R.error(Constant.TOKEN, "登录信息失效，请重新登陆");
        }
        CommunitycheckEntity communitycheckEntity = communitycheckService.getById(communitycheck.getRecId());
        //如果管理员同意则发送消息给超级管理员
        if (1 == communitycheck.getAdminState() && 1 == communitycheck.getNowState()) {
            //消息推送
            MessageUtli messageUtli = new MessageUtli(communitycheck.getCommunityId(), null, null, communitycheck.getCommunityName(), null, DateUtils.getLocalDateTimeStr(),
                    "admin", "admin", "superAdmin", "superAdmin", null, 10);
            sendMsg(messageUtli);
        }
        //如果被拒绝
        if (3 == communitycheck.getNowState()) {
            MessageUtli messageUtli = new MessageUtli(communitycheckEntity.getCommunityId(), null, null, communitycheckEntity.getCommunityName(), null, DateUtils.getLocalDateTimeStr(),
                    null, null, communitycheckEntity.getUserApplyId(), null, null, 3);
            sendMsg(messageUtli);
        } else if (0 == communitycheckEntity.getApplyType() && 1 == communitycheck.getSuperadminState() && 2 == communitycheck.getNowState()) {
            //新增社团人员表记录
            CommunityuserEntity communityuserEntity = communitycheckToCommunityuserEntity(communitycheckEntity);
            communityuserService.save(communityuserEntity);
            //新增社团人员流动表记录
            CommunityuserstreamEntity communityuserstreamEntity = communityuserstreamToCommunityuser(communitycheckEntity);
            communityuserstreamEntity.setApplyState(1);
            communityuserstreamService.save(communityuserstreamEntity);
            //成功后该社团状态恢复正常 人数+1
            CommunityEntity communityInfo = communityController.getCommunityInfo(communitycheckEntity.getCommunityId());
            communityInfo.setDevelopState(3);
            Integer newCommunityNum = communityInfo.getCommunityNumber() + 1;
            communityInfo.setCommunityNumber(newCommunityNum);
            communityService.updateById(communityInfo);
            //清除redis缓存  保证一致性
            redisUtil.del(Constant.COMMUNITY_ASSOCIATION + communitycheckEntity.getCommunityId());
            //redis中存入该社长
            redisUtil.set(Constant.COMMUNITY_POSITION_USERID + communitycheckEntity.getUserApplyId(), communitycheckEntity.getCommunityId());
            //消息推送
            MessageUtli messageUtli = new MessageUtli(communitycheckEntity.getCommunityId(), null, null, communitycheckEntity.getCommunityName(), null, DateUtils.getLocalDateTimeStr(),
                    "superAdmin", "superAdmin", communitycheckEntity.getUserApplyId(), communitycheckEntity.getUserApplyName(), null, 3);
            sendMsg(messageUtli);
            communitycheckService.updateById(communitycheck);
        }
        //如果换届审批通过，社团表该成员变为社长，社团人员关联表显示为社长，老社长自动退社，人员流动表更新
        else if (1 == communitycheckEntity.getApplyType() && 1 == communitycheck.getSuperadminState() && 2 == communitycheck.getNowState()) {
            //成功后该社团信息更改
            CommunityEntity community = communityController.getCommunityInfo(communitycheckEntity.getCommunityId());
            Integer newCommunityNum = community.getCommunityNumber() - 1;
            if (Objects.equals(community.getPeopleState(), 2)) {
                community.setPeopleState(0);
            }
            community.setCommunityPresident(communitycheckEntity.getNewPosition());
            community.setCommunityPresidentName(communitycheckEntity.getNewPositionName());
            community.setChanageNum(community.getChanageNum() + 1);
            community.setCommunityNumber(newCommunityNum);
            communityService.updateById(community);
            //清除redis缓存  保证一致性
            redisUtil.del(Constant.COMMUNITY_ASSOCIATION + communitycheckEntity.getCommunityId());
            //社团人员信息变动
            QueryWrapper<CommunityuserEntity> querwrapper = new QueryWrapper<>();
            querwrapper.eq("COMMUNITY_ID", communitycheckEntity.getCommunityId()).eq("USER_ID", communitycheckEntity.getUserApplyId());
            //原社长退社
            communityuserService.remove(querwrapper);
            //新社长职位改变
            QueryWrapper<CommunityuserEntity> querwrapper1 = new QueryWrapper<>();
            querwrapper1.eq("COMMUNITY_ID", communitycheckEntity.getCommunityId()).eq("USER_ID", communitycheckEntity.getNewPosition());
            CommunityuserEntity communityuserEntity = communityuserService.getOne(querwrapper1);
            communityuserEntity.setPosition(1);
            communityuserService.updateById(communityuserEntity);
            //新增社团人员流动表记录
            CommunityuserstreamEntity communityuserstreamEntity = communityuserstreamToCommunityuser(communitycheckEntity);
            communityuserstreamEntity.setOpState(0);
            communityuserstreamService.save(communityuserstreamEntity);
            //redis中存入该社长
            redisUtil.set(Constant.COMMUNITY_POSITION_USERID + communitycheckEntity.getNewPosition(), communitycheckEntity.getCommunityId());
            redisUtil.del(Constant.COMMUNITY_POSITION_USERID + communitycheckEntity.getUserApplyId());
            //消息推送给新社长
            MessageUtli messageUtli = new MessageUtli(communitycheckEntity.getCommunityId(), "你已成为" + communitycheckEntity.getCommunityName() + "社长", "换届成功通知", communitycheck.getCommunityName(), null, DateUtils.getLocalDateTimeStr(),
                    "superAdmin", "superAdmin", communitycheckEntity.getNewPosition(), communitycheckEntity.getNewPositionName(), null, 0);
            sendMsg(messageUtli);
            //消息推送给老社长
            MessageUtli messageUtli1 = new MessageUtli(communitycheckEntity.getCommunityId(), null, null, communitycheckEntity.getCommunityName(), null, DateUtils.getLocalDateTimeStr(),
                    "superAdmin", "superAdmin", communitycheckEntity.getUserApplyId(), communitycheckEntity.getUserApplyName(), null, 3);
            sendMsg(messageUtli1);
            communitycheckService.updateById(communitycheck);
        } else if (2 == communitycheckEntity.getApplyType() && 1 == communitycheck.getSuperadminState() && 2 == communitycheck.getNowState()) {
            //该社团状态逻辑删除
            CommunityEntity communityEntity = communityController.getCommunityInfo(communitycheckEntity.getCommunityId());
            communityService.removeById(communityEntity);
            QueryWrapper<CommunityuserEntity> querwrapper = new QueryWrapper<>();
            //查询出该社团人员
            querwrapper.eq("COMMUNITY_ID", communityEntity.getCommunityId());
            List<CommunityuserEntity> communityuserEntities = communityuserService.list(querwrapper);
            List<CommunityuserstreamEntity> communityuserstreamEntities = new ArrayList<CommunityuserstreamEntity>();
            for (int i = 0; i < communityuserEntities.size(); i++) {
                CommunityuserstreamEntity communityuserstreamEntity = communityuserController.communityUserToCommunityuserstreamEntity(communityuserEntities.get(i));
                //是社长  存入社长
                communityuserstreamEntity.setOpState(3);
                if (Objects.equals(communityuserEntities.get(i).getPosition(), 1)) {
                    communityuserstreamEntity.setPosition(1);
                }
                communityuserstreamEntities.add(communityuserstreamEntity);
            }
            //清空该社团的所有人员记录
            communityuserService.remove(querwrapper);
            //保存该社团的退社信息
            communityuserstreamService.saveBatch(communityuserstreamEntities);
            redisUtil.del(Constant.COMMUNITY_POSITION_USERID + communitycheckEntity.getUserApplyId());
            //消息推送给社长
            MessageUtli messageUtli = new MessageUtli(communitycheckEntity.getCommunityId(), null, null, communitycheckEntity.getCommunityName(), null, DateUtils.getLocalDateTimeStr(),
                    "superAdmin", "superAdmin", communitycheckEntity.getUserApplyId(), communitycheckEntity.getUserApplyName(), null, 3);
            sendMsg(messageUtli);
            //消息推送给所有人
            List list = new ArrayList();
            for (int i = 0; i < communityuserEntities.size(); i++) {
                if (communityuserEntities.get(i).getPosition() != 1) {
                    list.add(communityuserEntities.get(i).getUserId());
                }
            }
            MessageUtli messageUtli1 = new MessageUtli(communitycheckEntity.getCommunityId(), "您所在的" + communitycheckEntity.getCommunityName() + "已解散", "社团解散通知", communitycheckEntity.getCommunityName(), null, DateUtils.getLocalDateTimeStr(),
                    "superAdmin", "superAdmin", ConUtils.listToString(list), null, null, 0);
            sendMsg(messageUtli1);
        }
        communitycheckService.updateById(communitycheck);
        return R.ok();
    }


}
