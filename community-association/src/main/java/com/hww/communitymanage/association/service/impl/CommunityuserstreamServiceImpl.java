package com.hww.communitymanage.association.service.impl;

import com.hww.communitymanage.association.entity.CommunityuserEntity;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hww.common.utils.PageUtils;
import com.hww.common.utils.Query;

import com.hww.communitymanage.association.dao.CommunityuserstreamDao;
import com.hww.communitymanage.association.entity.CommunityuserstreamEntity;
import com.hww.communitymanage.association.service.CommunityuserstreamService;
import org.springframework.util.StringUtils;


@Service("communityuserstreamService")
public class CommunityuserstreamServiceImpl extends ServiceImpl<CommunityuserstreamDao, CommunityuserstreamEntity> implements CommunityuserstreamService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<CommunityuserstreamEntity> querwrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(params.get("userId"))){
            querwrapper.eq("USER_ID",params.get("userId"));
        }
        if (!StringUtils.isEmpty(params.get("communityId"))){
            querwrapper.eq("COMMUNITY_ID",params.get("communityId"));
        }
        if (!StringUtils.isEmpty(params.get("applyState"))){
            querwrapper.eq("APPLY_STATE",params.get("applyState"));
        }
        if (!StringUtils.isEmpty(params.get("position"))){
            querwrapper.eq("POSITION",params.get("position"));
        }
        IPage<CommunityuserstreamEntity> page = this.page(
                new Query<CommunityuserstreamEntity>().getPage(params),
               querwrapper
        );

        return new PageUtils(page);
    }

}