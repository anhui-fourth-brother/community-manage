package com.hww.communitymanage.association.dao;

import com.hww.communitymanage.association.entity.CommunityuserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 15:58:09
 */
@Mapper
public interface CommunityuserDao extends BaseMapper<CommunityuserEntity> {
	
}
