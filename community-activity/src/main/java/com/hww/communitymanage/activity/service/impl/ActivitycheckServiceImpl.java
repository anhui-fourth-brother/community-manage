package com.hww.communitymanage.activity.service.impl;

import com.hww.communitymanage.activity.entity.ActivityEntity;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hww.common.utils.PageUtils;
import com.hww.common.utils.Query;

import com.hww.communitymanage.activity.dao.ActivitycheckDao;
import com.hww.communitymanage.activity.entity.ActivitycheckEntity;
import com.hww.communitymanage.activity.service.ActivitycheckService;
import org.springframework.util.StringUtils;


@Service("activitycheckService")
public class ActivitycheckServiceImpl extends ServiceImpl<ActivitycheckDao, ActivitycheckEntity> implements ActivitycheckService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<ActivitycheckEntity> querwrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(params.get("communityId"))){
            querwrapper.eq("COMMUNITY_ID",params.get("communityId"));
        }
        if (!StringUtils.isEmpty(params.get("dealId"))){
            querwrapper.eq("DEAL_ID",params.get("dealId"));
        }
        if (!StringUtils.isEmpty(params.get("applyType"))) {
            querwrapper.eq("APPLY_TYPE", params.get("applyType"));
        }
        if (!StringUtils.isEmpty(params.get("applyId"))){
            querwrapper.eq("APPLY_ID",params.get("applyId"));
        }
        if (!StringUtils.isEmpty(params.get("nowState"))){
            querwrapper.eq("NOW_STATE",params.get("nowState"));
        }
        if (!StringUtils.isEmpty(params.get("typeId"))){
            querwrapper.eq("TYPE_ID",params.get("typeId"));
        }

        IPage<ActivitycheckEntity> page = this.page(
                new Query<ActivitycheckEntity>().getPage(params),
                querwrapper
        );

        return new PageUtils(page);
    }

}