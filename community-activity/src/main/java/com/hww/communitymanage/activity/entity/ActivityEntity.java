package com.hww.communitymanage.activity.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 15:53:07
 */
@Data
@TableName("act_activity")
public class ActivityEntity implements Serializable {
	private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_UUID)   //使用UUID策略来生成主键
    private String recId;
    /**
     * 活动id
     */
	@TableField(fill = FieldFill.INSERT) //随机生成ID
    private String activityId;
	/**
	 * 活动图片
	 */
	private String activityUrl;
	/**
	 * 活动名称
	 */
	private String activityName;
	/**
	 * 报名开始时间
	 */
	private String registerBeginTime;
	/**
	 * 报名结束时间
	 */
	private String registerEndTime;
	/**
	 * 活动结束时间
	 */
	private String endTime;
	/**
	 * 活动开始时间
	 */
	private String beginTime;
	/**
	 * 活动描述
	 */
	private String activityIntroduce;
	/**
	 * 活动情况0正常 1更改 （临时存储）
	 */
	private Integer activityState;
	/**
	 * 0报名未开始1报名进行中2报名已截止3活动进行中4活动已结束
	 */
	private Integer delpState;
	/**
	 * 活动地点
	 */
	private String activityPlace;
	/**
	 * 活动类别0线上1线下
	 */
	private Integer activityType;
	/**
	 * 活动当前状态0正常1人数已满2未开始3已结束
	 */
	private Integer nowState;
	/**
	 * 活动分数
	 */
	private Double activityScore;
	/**
	 * 创办人id
	 */
	private String applyUserId;
	/**
	 * 创办人姓名
	 */
	private String applyUserName;
	/**
	 * 创办社团/机构
	 */
	private String createOrganizationId;
	/**
	 * 创办时间
	 */
	private String createTime;
	/**
	 * 活动人数限制
	 */
	private Integer maxNum;
	/**
	 * 活动目前人数
	 */
	private Integer nowNum;
	/**
	 * 0社团活动1院级活动2校级活动
	 */
	private Integer developState;
    /**
     * 活动总结内容
     */
    private String comment;
    /**
     * 是否展示0不展示1展示
     */
    @TableLogic  //逻辑删除
    private Integer showState;
    /**
     * 热度（点击数）
     */
    private Integer hotNum;
	/**
	 * 最后更新时间
	 */
	private Object userinfo;
	/**
	 * 是否需要审批0不需要1需要
	 */
	private Integer isCheckState;
	/**
	 * 社团/机构名称
	 */
	private String createOrganizationName;
	/**
	 * 社团策划书
	 */
	private String url;
	/**
	 * 活动总结图片
	 */
	private String activityImages;

}
