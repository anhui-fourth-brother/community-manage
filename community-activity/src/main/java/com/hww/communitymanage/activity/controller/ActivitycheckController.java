package com.hww.communitymanage.activity.controller;

import java.time.LocalDateTime;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hww.common.utils.*;
import com.hww.communitymanage.activity.config.RedisUtil;
import com.hww.communitymanage.activity.entity.ActivityEntity;
import com.hww.communitymanage.activity.entity.ActivityuserEntity;
import com.hww.communitymanage.activity.entity.ActivityuserstreamEntity;
import com.hww.communitymanage.activity.service.ActivityService;
import com.hww.communitymanage.activity.service.ActivityuserService;
import com.hww.communitymanage.activity.service.ActivityuserstreamService;
import org.apache.commons.lang.StringUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import com.hww.communitymanage.activity.entity.ActivitycheckEntity;
import com.hww.communitymanage.activity.service.ActivitycheckService;


/**
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 15:53:07
 */
@RestController
@RequestMapping("activity/activitycheck")
public class ActivitycheckController {
    @Autowired
    private ActivitycheckService activitycheckService;
    @Autowired
    private ActivityuserService activityuserService;
    @Autowired
    private ActivityuserstreamService activityuserstreamService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private ActivityController activityController;
    @Autowired
    RedisUtil redisUtil;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * rabbit发送消息
     */
    private void sendMsg(Object msg) {
        String routingKey = "baseMsg";
        String content = JSON.toJSONString(msg);
        rabbitTemplate.convertAndSend(routingKey, content);
    }

    /**
     * 查询活动审批列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("activity:activitycheck:list")
    public R list(@RequestParam Map<String, Object> params,@RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN+token))){
            return R.error(Constant.TOKEN,"登录信息失效，请重新登陆");
        }
        PageUtils page = activitycheckService.queryPage(params);
        return R.ok().put("page", page);
    }

    /**
     * 根据主键查询活动审批信息
     */
    @RequestMapping("/info/{recId}")
    //@RequiresPermissions("activity:activitycheck:info")
    public R info(@PathVariable("recId") String recId) {
        ActivitycheckEntity activitycheck = activitycheckService.getById(recId);
        return R.ok().put("activitycheck", activitycheck);
    }
    //判断活动是否进入下一阶段  定时任务
//    @Scheduled(fixedRate = 1000*1)
//    public void updateActivityState() {
//        String beginNewKey=Constant.COMMUNITY_ACTIVITY_NEW_BEGIN+DateUtils.getLocalDateTimeStr();
//        String endNewKey=Constant.COMMUNITY_ACTIVITY_NEW_END+DateUtils.getLocalDateTimeStr();
//        String beginKey=Constant.COMMUNITY_ACTIVITY_NEW+DateUtils.getLocalDateTimeStr();
//        String endKey=Constant.COMMUNITY_ACTIVITY_END+DateUtils.getLocalDateTimeStr();
//        //如果活动结束报名
//      if (redisUtil.hasKey(endNewKey)){
//         String acvitityId =String.valueOf( redisUtil.get(endNewKey));
//          ActivityEntity activityEntity = activityController.getActivityInfo(acvitityId);
//          activityEntity.setDelpState(2);
//          activityService.removeById(activityEntity);
//          redisUtil.del(endNewKey);
//          redisUtil.del(Constant.COMMUNITY_ACTIVITY+acvitityId);
//      }
//        //如果活动开始
//        if (redisUtil.hasKey(beginKey)){
//            String acvitityId =String.valueOf( redisUtil.get(beginKey));
//            ActivityEntity activityEntity = activityController.getActivityInfo(acvitityId);
//            activityEntity.setDelpState(3);
//            activityService.removeById(activityEntity);
//            redisUtil.del(beginKey);
//            redisUtil.del(Constant.COMMUNITY_ACTIVITY+acvitityId);
//        }
//        //如果活动开始报名
//        if (redisUtil.hasKey(beginNewKey)){
//            String acvitityId =String.valueOf( redisUtil.get(beginNewKey));
//            ActivityEntity activityEntity = activityController.getActivityInfo(acvitityId);
//            activityEntity.setDelpState(1);
//            activityService.removeById(activityEntity);
//            redisUtil.del(beginNewKey);
//            redisUtil.del(Constant.COMMUNITY_ACTIVITY+acvitityId);
//        }
//        //如果活动结束报名
//        if (redisUtil.hasKey(endKey)){
//            String acvitityId =String.valueOf( redisUtil.get(endKey));
//            ActivityEntity activityEntity = activityController.getActivityInfo(acvitityId);
//            activityEntity.setDelpState(1);
//            activityService.removeById(activityEntity);
//            redisUtil.del(endKey);
//            redisUtil.del(Constant.COMMUNITY_ACTIVITY+acvitityId);
//        }
//      }

//    }

    /**
     * 活动审批
     */
    @RequestMapping("/save")
    //@RequiresPermissions("activity:activitycheck:save")
    public R save(@RequestBody ActivitycheckEntity activitycheck,@RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN+token))){
            return R.error(Constant.TOKEN,"登录信息失效，请重新登陆");
        }
        QueryWrapper<ActivitycheckEntity> querwrapper = new QueryWrapper<>();
        querwrapper.eq("COMMUNITY_ID", activitycheck.getCommunityId()).eq("ACTIVITY_ID", activitycheck.getActivityId()).ne("NOW_STATE", 2).ne("NOW_STATE", 3);
        ActivitycheckEntity activitycheckEntity = activitycheckService.getOne(querwrapper);
        if (activitycheckEntity != null) {
            return R.error(Constant.ERROR, "当前有活动正在处理中，请勿重复提交");
        }
        activitycheckService.save(activitycheck);
        if ("superAdmin".equals(activitycheck.getCommunityId())) {
            return R.ok();
        }
        //发送消息
        MessageUtli messageUtli = new MessageUtli(activitycheck.getCommunityId(), null, null, activitycheck.getCommunityName(), activitycheck.getActivityId(), DateUtils.getLocalDateTimeStr(), activitycheck.getApplyId(),
                activitycheck.getActivityName(), "admin", "admin", activitycheck.getActivityName(), 8);
        if ("admin".equals(activitycheck.getCommunityId())) {
            messageUtli.setAcceptUserId("superAdmin");
            messageUtli.setStateId(11);
        }
        sendMsg(messageUtli);

        return R.ok();
    }

    /**
     * 申批活动类申请
     */
    @RequestMapping("/update")
    //@RequiresPermissions("activity:activitycheck:update")
    public R update(@RequestBody ActivitycheckEntity activitycheck,@RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN+token))){
            return R.error(Constant.TOKEN,"登录信息失效，请重新登陆");
        }
        //先拿到社团审批表对象
        ActivitycheckEntity activitycheckEntity = activitycheckService.getById(activitycheck.getRecId());
        //对象转为活动人员对象
        ActivityuserEntity activityuserEntity = activityCheckToActivityuser(activitycheckEntity);
        //对象转为活动人员流动对象
        ActivityuserstreamEntity activityuserstreamEntity = activityCheckToActivityUserstream(activitycheckEntity);
        //拿到活动对象
        ActivityEntity activityEntity = activityController.getActivityInfo(activityuserstreamEntity.getActivityId());
        //创建消息类对象
        MessageUtli messageUtli = new MessageUtli(activityEntity.getCreateOrganizationId(), null, null, activityEntity.getCreateOrganizationName(), activityEntity.getActivityId(), DateUtils.getLocalDateTimeStr(),
                null, null, activitycheckEntity.getApplyId(), activitycheckEntity.getApplyName(), activitycheckEntity.getActivityName(), 2);
        //审批创建活动通过
        if ( activitycheck.getAdminState()!=null && activitycheck.getAdminState()==1){
            messageUtli.setAcceptUserId("superAdmin");
            messageUtli.setAcceptName("superAdmin");
            messageUtli.setStateId(11);
        }
        if (Objects.equals(activitycheckEntity.getApplyType(), 0) && Objects.equals(activitycheck.getSuperAdminState(), 1)) {
            //活动人员中添加申请人
            if (!"admin".equals(activitycheckEntity.getApplyId())){
            activityuserService.save(activityuserEntity);
            //新增活动人员流动表
            activityuserstreamService.save(activityuserstreamEntity);
                //活动当前人数+1
            activityEntity.setNowNum(activityEntity.getNowNum() + 1);
            }
            activityEntity.setActivityState(0);
            activityService.updateById(activityEntity);
            //解除该社团活动占用
            redisUtil.decr(Constant.COMMUNITY_ACTIVITY_NUM + activityEntity.getActivityId(), 1);
            //社团活动数+1
            redisUtil.incr(Constant.COMMUNITY_ACT_NUM + activityEntity.getCreateOrganizationId(), 1);
        }
        //审批修改活动通过
        else if (Objects.equals(activitycheckEntity.getApplyType(), 1) && Objects.equals(activitycheck.getSuperAdminState(), 1)) {
            QueryWrapper<ActivityEntity> querwrapper = new QueryWrapper<>();
            querwrapper.eq("ACTIVITY_ID", activitycheck.getActivityId()).eq("ACTIVITY_STATE", 1);
            ActivityEntity newActivity = activityService.getOne(querwrapper);
            activityService.removeById(newActivity.getRecId());
            newActivity.setRecId(activityEntity.getRecId());
            newActivity.setNowState(0);
            newActivity.setActivityState(0);
            activityService.updateById(newActivity);
            //解除该社团活动占用
            redisUtil.decr(Constant.COMMUNITY_ACTIVITY_NUM + activityEntity.getActivityId(), 1);
        }
        //活动结束
        else if (Objects.equals(activitycheckEntity.getApplyType(), 2) && Objects.equals(activitycheck.getSuperAdminState(), 1)) {
            activityEntity.setNowState(2);
            activityEntity.setActivityImages(activitycheckEntity.getUrl());
            activityEntity.setComment(activitycheckEntity.getApplyAdvice());
            activityService.updateById(activityEntity);
            QueryWrapper<ActivityuserEntity> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("ACTIVITY_ID", activityuserEntity.getActivityId()).eq("IS_SCORE", 0);
            List<ActivityuserEntity> list = activityuserService.list(queryWrapper);
            List <String>userList=new ArrayList();
            for (int i = 0; i < list.size(); i++) {
                //学生二课分数改变
                String key = Constant.COMMUNITY_USER_SCORE + list.get(i).getUserId();
               Double oldScore= Double.parseDouble((String.valueOf(redisUtil.get(key))));
                Double newScore = activityEntity.getActivityScore() + oldScore;
                redisUtil.set(key, newScore, 60 * 60 * 24 * 30 * 12 * 4);
                userList.add(list.get(i).getUserId());
                list.get(i).setScoreState(1);
            }
            //修改活动得分状态
            activityuserService.updateBatchById(list);
            messageUtli.setAcceptUserId(ConUtils.listToString(userList));
            messageUtli.setStateId(0);
            messageUtli.setContent(activityEntity.getActivityName()+"活动结束通知");
            messageUtli.setTitle("活动结束");
            //解除该社团活动占用
            redisUtil.decr(Constant.COMMUNITY_ACTIVITY_NUM + activityEntity.getActivityId(), 1);
        }
        //保证缓存一致性
        redisUtil.del(Constant.COMMUNITY_ACTIVITY + activityEntity.getActivityId());
        activitycheckService.updateById(activitycheck);
        //发送消息
        sendMsg(messageUtli);
        return R.ok();
    }

    //活动审批转为活动人员流动表
    private ActivityuserstreamEntity activityCheckToActivityUserstream(ActivitycheckEntity activitycheckEntity) {
        ActivityuserstreamEntity activityuserstreamEntity = new ActivityuserstreamEntity();
        activityuserstreamEntity.setActivityName(activitycheckEntity.getActivityName());
        activityuserstreamEntity.setActivityId(activitycheckEntity.getActivityId());
        activityuserstreamEntity.setUserName(activitycheckEntity.getApplyName());
        activityuserstreamEntity.setUserId(activitycheckEntity.getApplyId());
        activityuserstreamEntity.setOpTime(DateUtils.getLocalDateTimeStr());
        activityuserstreamEntity.setOpName(activitycheckEntity.getDealName());
        activityuserstreamEntity.setOpUser(activitycheckEntity.getDealId());
        activityuserstreamEntity.setDealState(2);
        activityuserstreamEntity.setApplyType(1);
        activityuserstreamEntity.setApplyTime(DateUtils.getLocalDateTimeStr());
        return activityuserstreamEntity;
    }

    //    //如果活动未开始修改了时间
//    public void updateActivity(ActivityEntity oldActivityEntity,ActivityEntity  newActivityEntity){
//        //如果到了此活动的活动开始时间则改变状态码
//        if (!Objects.equals(oldActivityEntity.getBeginTime(),newActivityEntity.getBeginTime())){
//           redisUtil.del(Constant.COMMUNITY_ACTIVITY_NEW+oldActivityEntity.getBeginTime());
//           redisUtil.set(Constant.COMMUNITY_ACTIVITY_NEW+newActivityEntity.getBeginTime(),newActivityEntity.getActivityId());
//       }
//        if (!Objects.equals(oldActivityEntity.getEndTime(),newActivityEntity.getEndTime())){
//           redisUtil.del(Constant.COMMUNITY_ACTIVITY_END+oldActivityEntity.getEndTime());
//           redisUtil.set(Constant.COMMUNITY_ACTIVITY_END+newActivityEntity.getEndTime(),newActivityEntity.getEndTime());
//       }
//        if (!Objects.equals(oldActivityEntity.getRegisterBeginTime(),newActivityEntity.getRegisterBeginTime())){
//           redisUtil.del(Constant.COMMUNITY_ACTIVITY_NEW_BEGIN+oldActivityEntity.getRegisterBeginTime());
//           redisUtil.set(Constant.COMMUNITY_ACTIVITY_NEW_BEGIN+newActivityEntity.getRegisterBeginTime(),newActivityEntity.getActivityId());
//       }
//        if (!Objects.equals(oldActivityEntity.getRegisterEndTime(),newActivityEntity.getRegisterEndTime())){
//           redisUtil.del(Constant.COMMUNITY_ACTIVITY_NEW_END+oldActivityEntity.getRegisterEndTime());
//           redisUtil.set(Constant.COMMUNITY_ACTIVITY_NEW_END+newActivityEntity.getRegisterEndTime(),newActivityEntity.getActivityId());
//       }
//    }
    //活动审批表对象转活动人员表对象
    private ActivityuserEntity activityCheckToActivityuser(ActivitycheckEntity activitycheckEntity) {
        ActivityuserEntity activityuserEntity = new ActivityuserEntity();
        activityuserEntity.setActivityId(activitycheckEntity.getActivityId());
        activityuserEntity.setActivityName(activitycheckEntity.getActivityName());
        activityuserEntity.setUserId(activitycheckEntity.getApplyId());
        activityuserEntity.setUserName(activitycheckEntity.getApplyName());
        activityuserEntity.setJoinTime(DateUtils.getLocalDateTimeStr());
        return activityuserEntity;
    }


}
