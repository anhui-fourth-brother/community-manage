package com.hww.communitymanage.activity.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hww.common.utils.*;
import com.hww.communitymanage.activity.config.RedisUtil;
import com.hww.communitymanage.activity.entity.ActivityEntity;
import com.hww.communitymanage.activity.entity.ActivityuserEntity;
import com.hww.communitymanage.activity.feign.ActivityFeignService;
import com.hww.communitymanage.activity.service.ActivityService;
import com.hww.communitymanage.activity.service.ActivityuserService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import com.hww.communitymanage.activity.entity.ActivityuserstreamEntity;
import com.hww.communitymanage.activity.service.ActivityuserstreamService;


/**
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 15:53:07
 */
@RestController
@RequestMapping("activity/activityuserstream")
public class ActivityuserstreamController {
    @Autowired
    private ActivityuserstreamService activityuserstreamService;
    @Autowired
    private ActivityuserService activityuserService;
    @Autowired
    private ActivityController activityController;
    @Autowired
    private ActivityService activityService;
    @Autowired
    RedisUtil redisUtil;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private ActivityFeignService activityFeignService;

    /**
     * rabbit发送消息
     */
    private void sendMsg(Object msg) {
        String routingKey = "baseMsg";
        String content = JSON.toJSONString(msg);
        rabbitTemplate.convertAndSend(routingKey, content);
    }

    /**
     * 查询活动人员流动信息
     */
    @RequestMapping("/list")
    //@RequiresPermissions("activity:activityuserstream:list")
    public R list(@RequestParam Map<String, Object> params, @RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN + token))) {
            return R.error(Constant.TOKEN, "登录信息失效，请重新登陆");
        }
        PageUtils page = activityuserstreamService.queryPage(params);

        return R.ok().put("page", page);
    }


//    /**
//     * 信息
//     */
//    @RequestMapping("/info/{recId}")
//    //@RequiresPermissions("activity:activityuserstream:info")
//    public R info(@PathVariable("recId") String recId){
//		ActivityuserstreamEntity activityuserstream = activityuserstreamService.getById(recId);
//
//        return R.ok().put("activityuserstream", activityuserstream);
//    }

    /**
     * 学生申请加入/退出活动
     */
    @RequestMapping("/save")
    //@RequiresPermissions("activity:activityuserstream:save")
    public R save(@RequestBody ActivityuserstreamEntity activityuserstream, @RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN + token))) {
            return R.error(Constant.TOKEN, "登录信息失效，请重新登陆");
        }
        QueryWrapper<ActivityuserstreamEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("DEAL_STATE", 0).eq("ACTIVITY_ID", activityuserstream.getActivityId()).eq("USER_ID", activityuserstream.getUserId());
        List<ActivityuserstreamEntity> activityuserstreamEntityList = activityuserstreamService.list(queryWrapper);
        if (activityuserstreamEntityList.size() != 0) {
            return R.error(Constant.ERROR, "请求处理中，请勿重复提交");
        }
        ActivityEntity activityInfo = activityController.getActivityInfo(activityuserstream.getActivityId());
        if (activityInfo.getDevelopState() == 0) {
            Map<String, Object> map = new HashMap();
            map.put("userId", activityuserstream.getUserId());
            map.put("communityId", activityInfo.getCreateOrganizationId());
            Boolean result = activityFeignService.getResult(map);
            if (!result) {
                return R.error(Constant.ERROR, "你不是该社团的成员哦，无法参加其社团活动");
            }
        }
        //如果该人员已在活动中
        QueryWrapper<ActivityuserEntity> queryWrapper2 = new QueryWrapper<>();
        queryWrapper2.eq("ACTIVITY_ID",activityuserstream.getActivityId()).eq("USER_ID",activityuserstream.getUserId());
        List<ActivityuserEntity> activityuserEntityList = activityuserService.list(queryWrapper2);
        if (activityuserEntityList.size() != 0 && activityuserstream.getApplyType()==1) {
            return R.error(Constant.ERROR, "你已参加该活动，请勿重复申请");
        }
        //创建消息类对象
        MessageUtli messageUtli = new MessageUtli(null, null, null, null, activityuserstream.getActivityId(), DateUtils.getLocalDateTimeStr(),
                null, null, activityInfo.getApplyUserId(), activityInfo.getApplyUserName(), activityuserstream.getActivityName(), 6);
        //如果活动免审批，则不用审批
        if (activityuserstream.getApplyType() == 1 && activityInfo.getIsCheckState() == 0) {
            activityuserstream.setDealState(2);
            activityuserstream.setOpUser(activityInfo.getApplyUserId());
            activityuserstream.setOpName(activityInfo.getApplyUserName());
            activityuserstream.setOpTime(DateUtils.getLocalDateTimeStr());
            ActivityuserEntity activityuserEntity = activityUserStreamToActivityUser(activityuserstream);
            if (activityInfo.getNowState() == 1) {
                return R.error(Constant.ERROR, "该活动人数已满");
            }
            activityuserService.save(activityuserEntity);
            //如果目前人数已满
            if (activityInfo.getNowNum() + 1 == activityInfo.getMaxNum()) {
                activityInfo.setNowState(1);
            }
            activityInfo.setNowNum(activityInfo.getNowNum() + 1);
            //更改活动状态
            activityService.updateById(activityInfo);
            //删除缓存消息
            redisUtil.del(Constant.COMMUNITY_ACTIVITY + activityInfo.getActivityId());
            //直接向申请人发送消息
            messageUtli.setStateId(2);
            messageUtli.setAcceptUserId(activityuserstream.getUserId());
            sendMsg(messageUtli);
        }
        if ("admin".equals(activityInfo.getCreateOrganizationId())){
            messageUtli.setAcceptUserId("admin");
            activityuserstream.setOpUser("admin");
            activityuserstream.setOpName("admin");
        }
        sendMsg(messageUtli);
        activityuserstreamService.save(activityuserstream);
        return R.ok();
    }

    /**
     * 审批学生加退活动
     */
    @RequestMapping("/update")
    //@RequiresPermissions("activity:activityuserstream:update")
    public R update(@RequestBody ActivityuserstreamEntity activityuserstream, @RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN + token))) {
            return R.error(Constant.TOKEN, "登录信息失效，请重新登陆");
        }
        ActivityuserstreamEntity activityuserstreamEntity = activityuserstreamService.getById(activityuserstream.getRecId());
        ActivityEntity activityInfo = activityController.getActivityInfo(activityuserstreamEntity.getActivityId());
        activityuserstreamService.updateById(activityuserstream);
        //创建消息类对象
        MessageUtli messageUtli = new MessageUtli(null, null, null, null, activityuserstreamEntity.getActivityId(), DateUtils.getLocalDateTimeStr(),
                null, null, activityuserstreamEntity.getUserId(), activityuserstreamEntity.getUserName(), activityuserstreamEntity.getActivityName(), 2);
        //如果同意其退出活动
        if (2 == activityuserstream.getDealState() && activityuserstreamEntity.getApplyType() == 0) {
            QueryWrapper<ActivityuserEntity> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("ACTIVITY_ID", activityuserstreamEntity.getActivityId()).eq("USER_ID", activityuserstreamEntity.getUserId());
            activityInfo.setNowNum(activityInfo.getNowNum() - 1);
            activityuserService.remove(queryWrapper);
            //如果当前人数已满
            if (activityInfo.getNowState() == 1) {
                activityInfo.setNowState(0);
            }
            activityService.updateById(activityInfo);
            //删除缓存消息
            redisUtil.del(Constant.COMMUNITY_ACTIVITY + activityInfo.getActivityId());
        }
        //如果同意其加入活动
        if (2 == activityuserstream.getDealState() && activityuserstreamEntity.getApplyType() == 1) {
            ActivityuserEntity activityuserEntity = activityUserStreamToActivityUser(activityuserstreamEntity);
            if (activityInfo.getNowState() == 1) {
                return R.error(Constant.ERROR, "该活动人数已满");
            }
            activityuserService.save(activityuserEntity);
            //如果目前人数已满
            if (activityInfo.getNowNum() + 1 == activityInfo.getMaxNum()) {
                activityInfo.setNowState(1);
            }
            activityInfo.setNowNum(activityInfo.getNowNum() + 1);
            //更改活动状态
            activityService.updateById(activityInfo);
            //删除缓存消息
            redisUtil.del(Constant.COMMUNITY_ACTIVITY + activityInfo.getActivityId());
        }
        sendMsg(messageUtli);
        return R.ok();
    }

    /**
     * 活动人员流动转为活动人员
     *
     * @param activityuserstreamEntity
     * @return ActivityuserEntity
     */
    private ActivityuserEntity activityUserStreamToActivityUser(ActivityuserstreamEntity activityuserstreamEntity) {
        ActivityuserEntity activityuserEntity = new ActivityuserEntity();
        activityuserEntity.setActivityId(activityuserstreamEntity.getActivityId());
        activityuserEntity.setActivityName(activityuserstreamEntity.getActivityName());
        activityuserEntity.setJoinTime(DateUtils.getLocalDateTimeStr());
        activityuserEntity.setUserId(activityuserstreamEntity.getUserId());
        activityuserEntity.setUserName(activityuserstreamEntity.getUserName());
        return activityuserEntity;
    }

//    /**
//     * 删除
//     */
//    @RequestMapping("/delete")
//    //@RequiresPermissions("activity:activityuserstream:delete")
//    public R delete(@RequestBody String[] recIds){
//		activityuserstreamService.removeByIds(Arrays.asList(recIds));
//
//        return R.ok();
//    }

}
