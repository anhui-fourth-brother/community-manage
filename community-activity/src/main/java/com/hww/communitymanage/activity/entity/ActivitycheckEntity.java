package com.hww.communitymanage.activity.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 15:53:07
 */
@Data
@TableName("act_activitycheck")
public class ActivitycheckEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.ASSIGN_UUID)   //使用UUID策略来生成主键
	private String recId;
	/**
	 * 活动id
	 */
	private String activityId;
	/**
	 * 活动名称
	 */
	private String activityName;
	/**
	 * 申请人id
	 */
	private String applyId;
	/**
	 * 申请人姓名
	 */
	private String applyName;
	/**
	 * 社团id
	 */
	private String communityId;
	/**
	 * 社团姓名
	 */
	private String communityName;
	/**
	 * 审批人id
	 */
	private String dealId;
	/**
	 * 审批人姓名
	 */
	private String dealName;
	/**
	 * 申请时间
	 */
	private String applyTime;
	/**
	 * 附件路径
	 */
	private String url;
	/**
	 * 审批类型0创办活动1修改活动2活动结束申请
	 */
	private Integer applyType;
	/**
	 * 审批意见
	 */

	private String advice;
	/**
	 * 活动流程0发起1管理员处理2 超级管理员3结束
	 */
	private Integer nowState;
	/**
	 * 审批状态0未处理1拒绝2通过
	 */
	private Integer adminState;
	/**
	 * 审批状态0未处理1拒绝2通过
	 */
	private String adminTime;
	/**
	 * 审批状态0未处理1拒绝2通过
	 */
	private Integer superAdminState;
	/**
	 * 超级管理员审批时间
	 */
	private String superAdminTime;
	/**
	 * 最后更新时间
	 */
	private String applyAdvice;
	/**
	 * 0社团活动1院级活动2校级活动
	 */
	private Integer typeId;
	/**
	 * 超级管理员处理意见
	 */
	private String superadminAdvice;
	/**
	 * 乐观锁字段
	 */
	@Version
	private Integer version;

}
