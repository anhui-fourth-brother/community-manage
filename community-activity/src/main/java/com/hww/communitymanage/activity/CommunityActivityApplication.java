package com.hww.communitymanage.activity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.scheduling.annotation.EnableScheduling;


@EnableScheduling
@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients(basePackages = "com.hww.communitymanage.activity.feign")
public class CommunityActivityApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommunityActivityApplication.class, args);
    }
}