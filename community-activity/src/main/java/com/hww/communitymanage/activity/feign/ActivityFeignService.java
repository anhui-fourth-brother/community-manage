package com.hww.communitymanage.activity.feign;


import com.hww.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;


/**
 * 这是一个声明式的远程调用
 */
@FeignClient("community-association")
public interface ActivityFeignService {
    @RequestMapping("association/communityuser/isCommunityUser")
    //判断该人员是否在社团中
    public Boolean getResult(Map<String, Object> map);
}
