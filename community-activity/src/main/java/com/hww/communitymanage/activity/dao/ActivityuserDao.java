package com.hww.communitymanage.activity.dao;

import com.hww.communitymanage.activity.entity.ActivityuserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 15:53:07
 */
@Mapper
public interface ActivityuserDao extends BaseMapper<ActivityuserEntity> {
	
}
