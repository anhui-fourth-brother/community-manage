package com.hww.communitymanage.activity.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.Objects;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hww.common.utils.PageUtils;
import com.hww.common.utils.Query;

import com.hww.communitymanage.activity.dao.ActivityDao;
import com.hww.communitymanage.activity.entity.ActivityEntity;
import com.hww.communitymanage.activity.service.ActivityService;
import org.springframework.util.StringUtils;


@Service("activityService")
public class ActivityServiceImpl extends ServiceImpl<ActivityDao, ActivityEntity> implements ActivityService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<ActivityEntity> querwrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(params.get("activityId"))){
            querwrapper.eq("ACTIVITY_ID",params.get("activityId"));
        }
        if (!StringUtils.isEmpty(params.get("developState"))){
            querwrapper.eq("DEVELOP_STATE",params.get("developState"));
        }
        if (!StringUtils.isEmpty(params.get("activityName"))){
            querwrapper.like("ACTIVITY_NAME",params.get("activityName"));
        }
        if (!StringUtils.isEmpty(params.get("nowState"))){
            querwrapper.eq("NOW_STATE",params.get("nowState"));
        }
        if (!StringUtils.isEmpty(params.get("activityType"))){
            querwrapper.eq("ACTIVITY_TYPE",params.get("activityType"));
        }
        if (!StringUtils.isEmpty(params.get("activityState"))){
            querwrapper.eq("ACTIVITY_STATE",0);
        }
        if (!StringUtils.isEmpty(params.get("delpState"))){
            querwrapper.eq("DELP_STATE",params.get("delpState"));
        }
        if (!StringUtils.isEmpty(params.get("createOrganizationId"))){
            querwrapper.eq("CREATE_ORGANIZATION_ID",params.get("createOrganizationId"));
        }
        querwrapper.eq("ACTIVITY_STATE",0);

        IPage<ActivityEntity> page = this.page(
                new Query<ActivityEntity>().getPage(params),
                querwrapper
        );

        return new PageUtils(page);
    }

}