package com.hww.communitymanage.activity.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hww.common.utils.PageUtils;
import com.hww.common.utils.Query;

import com.hww.communitymanage.activity.dao.ActivityuserstreamDao;
import com.hww.communitymanage.activity.entity.ActivityuserstreamEntity;
import com.hww.communitymanage.activity.service.ActivityuserstreamService;
import org.springframework.util.StringUtils;


@Service("activityuserstreamService")
public class ActivityuserstreamServiceImpl extends ServiceImpl<ActivityuserstreamDao, ActivityuserstreamEntity> implements ActivityuserstreamService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<ActivityuserstreamEntity> querwrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(params.get("opUser"))){
            querwrapper.eq("OP_USER",params.get("opUser"));
        }
        if (!StringUtils.isEmpty(params.get("userId"))){
            querwrapper.eq("USER_ID",params.get("userId"));
        }
        if (!StringUtils.isEmpty(params.get("activityId"))){
            querwrapper.eq("ACTIVITY_ID",params.get("activityId"));
        }

        IPage<ActivityuserstreamEntity> page = this.page(
                new Query<ActivityuserstreamEntity>().getPage(params),
                querwrapper
        );

        return new PageUtils(page);
    }

}