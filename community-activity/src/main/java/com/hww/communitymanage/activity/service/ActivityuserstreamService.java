package com.hww.communitymanage.activity.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hww.common.utils.PageUtils;
import com.hww.communitymanage.activity.entity.ActivityuserstreamEntity;

import java.util.Map;

/**
 * 
 *
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 15:53:07
 */
public interface ActivityuserstreamService extends IService<ActivityuserstreamEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

