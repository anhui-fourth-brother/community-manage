package com.hww.communitymanage.activity.controller;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hww.common.utils.*;
import com.hww.communitymanage.activity.config.RedisUtil;
import com.hww.communitymanage.activity.entity.ActivitycheckEntity;
import com.hww.communitymanage.activity.entity.ActivityuserEntity;
import com.hww.communitymanage.activity.entity.ActivityuserstreamEntity;
import com.hww.communitymanage.activity.service.ActivitycheckService;
import com.hww.communitymanage.activity.service.ActivityuserService;
import com.hww.communitymanage.activity.service.ActivityuserstreamService;
import org.apache.commons.lang.StringUtils;
import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RedissonClient;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import com.hww.communitymanage.activity.entity.ActivityEntity;
import com.hww.communitymanage.activity.service.ActivityService;


/**
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 15:53:07
 */
@RestController
@RequestMapping("activity/activity")
public class ActivityController {
    @Autowired
    private ActivityService activityService;
    @Autowired
    private ActivityuserService activityuserService;
    @Autowired
    private ActivitycheckService activitycheckService;
    @Autowired
    private ActivityuserstreamService activityuserstreamService;

    @Autowired(required = false)
    Redisson redisson;
    @Autowired
    RedisUtil redisUtil;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * rabbit发送消息
     */
    private void sendMsg(Object msg) {
        String routingKey = "baseMsg";
        String content = JSON.toJSONString(msg);
        rabbitTemplate.convertAndSend(routingKey, content);
    }
    //定时任务，每天更新一次活动总数
    @Scheduled(fixedRate = 60*60*24)
    public void updateActivityNm() {
        redisUtil.set(Constant.ACTIVITY_NUM, activityService.count(),60*60*24*2);
    }
    /**
     * 按条件查询活动列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("activity:activity:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = activityService.queryPage(params);
        List<ActivityEntity> list = (List<ActivityEntity>) page.getList();
        for (int i = 0; i < list.size(); i++) {
            //活动报名开始时间
            if (1 > list.get(i).getRegisterBeginTime().compareTo(DateUtils.getLocalDateTimeStr()) && list.get(i).getDelpState() < 1) {
                ActivityEntity activityEntity = list.get(i);
                activityEntity.setDelpState(1);
                activityService.updateById(activityEntity);
                redisUtil.del(Constant.COMMUNITY_ACTIVITY + activityEntity.getActivityId());
            }
            //活动报名结束时间
            if (1 > list.get(i).getRegisterEndTime().compareTo(DateUtils.getLocalDateTimeStr()) && list.get(i).getDelpState() < 2) {
                ActivityEntity activityEntity = list.get(i);
                activityEntity.setDelpState(2);
                activityService.updateById(activityEntity);
                redisUtil.del(Constant.COMMUNITY_ACTIVITY + activityEntity.getActivityId());
            }
            //活动开始时间
            if (1 > list.get(i).getBeginTime().compareTo(DateUtils.getLocalDateTimeStr()) && list.get(i).getDelpState() < 3) {
                ActivityEntity activityEntity = list.get(i);
                activityEntity.setDelpState(3);
                activityService.updateById(activityEntity);
                redisUtil.del(Constant.COMMUNITY_ACTIVITY + activityEntity.getActivityId());
            }
            //活动结束时间
            if (1 > list.get(i).getEndTime().compareTo(DateUtils.getLocalDateTimeStr()) && list.get(i).getDelpState() < 4) {
                ActivityEntity activityEntity = list.get(i);
                activityEntity.setDelpState(4);
                activityService.updateById(activityEntity);
                redisUtil.del(Constant.COMMUNITY_ACTIVITY + activityEntity.getActivityId());
            }
        }
        return R.ok().put("page", page);
    }

//
//    /**
//     * 信息
//     */
//    @RequestMapping("/info/{recId}")
//    //@RequiresPermissions("activity:activity:info")
//    public R info(@PathVariable("recId") String recId){
//		ActivityEntity activity = activityService.getById(recId);
//
//        return R.ok().put("activity", activity);
//    }

    /**
     * 申请/创建/修改活动
     */
    @RequestMapping("/save")
    //@RequiresPermissions("activity:activity:save")
    public R save(@RequestBody ActivityEntity activity,@RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN+token))){
            return R.error(Constant.TOKEN,"登录信息失效，请重新登陆");
        }
        QueryWrapper<ActivitycheckEntity> querwrapper = new QueryWrapper<>();
        querwrapper.eq("APPLY_ID", activity.getApplyUserId()).ne("SUPER_ADMIN_STATE", 1).ne("NOW_STATE", 3);
        ActivitycheckEntity activitycheckEntity = activitycheckService.getOne(querwrapper);
        if (activitycheckEntity != null) {
            return R.error(Constant.ERROR, "该活动请求正在处理中，请勿重复提交");
        }
        //创建消息类对象
        MessageUtli messageUtli = new MessageUtli(activity.getCreateOrganizationId(), null, null, activity.getCreateOrganizationName(), activity.getActivityId(), DateUtils.getLocalDateTimeStr(),
                null, null, "admin", "admin", activity.getActivityName(), 8);

        //id为空  生成活动ID
        if (StringUtils.isEmpty(activity.getActivityId())) {
            String activityId = IdUtils.getIncreaseIdByNanoTime();
            activity.setActivityId(activityId);
        }
        //保存该活动
        activityService.save(activity);
        //是超级管理员发布的活动  无需审核  
        if ("superAdmin".equals(activity.getCreateOrganizationId())) {
//            ActivitycheckEntity activitycheckEntity1 = activityToActivitycheck(activity);
//            activitycheckEntity1.setApplyId(activity.getCreateOrganizationId());
//            activitycheckEntity1.setSuperAdminState(1);
//            activitycheckEntity1.setNowState(2);
//            activitycheckEntity1.setSuperAdminTime(DateUtils.getLocalDateTimeStr());
//            activitycheckService.save(activitycheckEntity1);
            return R.ok();
        }
        //是管理员发布的活动  无需管理员审核
        else if ("admin".equals(activity.getCreateOrganizationId())) {
            ActivitycheckEntity activitycheckEntity1 = activityToActivitycheck(activity);
            activitycheckEntity1.setApplyId(activity.getCreateOrganizationId());
            activitycheckEntity1.setNowState(1);
            activitycheckEntity1.setAdminState(1);
            activitycheckEntity1.setAdminTime(DateUtils.getLocalDateTimeStr());
            activitycheckService.save(activitycheckEntity1);
            //处理消息
            messageUtli.setStateId(11);
            messageUtli.setAcceptUserId("superAdmin");
            messageUtli.setAcceptName("superAdmin");
        } else {
            ActivitycheckEntity activitycheckEntity1 = activityToActivitycheck(activity);
            //如果是修改活动信息，暂停该活动招聘
            if (activity.getActivityState() == 1) {
                ActivityEntity activityInfo = getActivityInfo(activity.getActivityId());
                activityInfo.setNowState(2);
                activitycheckEntity1.setApplyType(1);
                activityService.updateById(activityInfo);
                redisUtil.del(Constant.COMMUNITY_ACTIVITY + activityInfo.getActivityId());
            }
            activitycheckService.save(activitycheckEntity1);
        }
        sendMsg(messageUtli);
        //活动进行中
        redisUtil.incr(Constant.COMMUNITY_ACTIVITY_NUM + activity.getCreateOrganizationId(), 1);
        return R.ok();
    }

    //活动表转换为审批表
    private ActivitycheckEntity activityToActivitycheck(ActivityEntity activity) {
        ActivitycheckEntity activitycheckEntity = new ActivitycheckEntity();
        activitycheckEntity.setActivityId(activity.getActivityId());
        activitycheckEntity.setApplyId(activity.getApplyUserId());
        activitycheckEntity.setApplyName(activity.getApplyUserName());
        activitycheckEntity.setCommunityName(activity.getCreateOrganizationName());
        activitycheckEntity.setCommunityId(activity.getCreateOrganizationId());
        activitycheckEntity.setApplyTime(DateUtils.getLocalDateTimeStr());
        activitycheckEntity.setUrl(activity.getUrl());
        activitycheckEntity.setTypeId(activity.getDevelopState());
        activitycheckEntity.setActivityName(activity.getActivityName());
        return activitycheckEntity;
    }

    /**
     * 修改活动信息
     */
    @RequestMapping("/update")
    //@RequiresPermissions("activity:activity:update")
    public R update(@RequestBody ActivityEntity activity,@RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN+token))){
            return R.error(Constant.TOKEN,"登录信息失效，请重新登陆");
        }

        //超级管理员活动总结
        if (StringUtils.isNotEmpty(activity.getActivityImages())){
            ActivityEntity activityEntity=activityService.getById(activity.getRecId());
            QueryWrapper<ActivityuserEntity> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("ACTIVITY_ID", activityEntity.getActivityId()).eq("IS_SCORE", 0);
            List<ActivityuserEntity> list = activityuserService.list(queryWrapper);
            List <String>userList=new ArrayList();
            for (int i = 0; i < list.size(); i++) {
                //学生二课分数改变
                String key = Constant.COMMUNITY_USER_SCORE + list.get(i).getUserId();
                Double oldScore= Double.parseDouble((String.valueOf(redisUtil.get(key))));
                Double newScore = activityEntity.getActivityScore() + oldScore;
                redisUtil.set(key, newScore, 60 * 60 * 24 * 30 * 12 * 4);
                userList.add(list.get(i).getUserId());
                list.get(i).setScoreState(1);
            }
            //创建消息类对象
            MessageUtli messageUtli = new MessageUtli(activityEntity.getCreateOrganizationId(), activityEntity.getActivityName()+"活动结束通知", "活动结束", activityEntity.getCreateOrganizationName(), activityEntity.getActivityId(), DateUtils.getLocalDateTimeStr(),
                    null, null, ConUtils.listToString(userList), null, activityEntity.getActivityName(), 0);
            //修改活动得分状态
            activityuserService.updateBatchById(list);
            //解除该社团活动占用
            redisUtil.decr(Constant.COMMUNITY_ACTIVITY_NUM + activityEntity.getActivityId(), 1);
            //缓存一致性
            redisUtil.del(Constant.COMMUNITY_ACTIVITY + activityEntity.getActivityId());
        }
        activityService.updateById(activity);
        return R.ok();
    }

    /**
     * 删除活动
     */
    @RequestMapping("/delete/{recId}")
    //@RequiresPermissions("activity:activity:delete")
    public R delete(@PathVariable("recId") String recId,@RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN+token))){
            return R.error(Constant.TOKEN,"登录信息失效，请重新登陆");
        }
        ActivityEntity activityEntity = activityService.getById(recId);
        activityService.removeById(recId);
        QueryWrapper<ActivityuserEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("ACTIVITY_ID", activityEntity.getActivityId());
        List<ActivityuserEntity> list = activityuserService.list(queryWrapper);
        List<ActivityuserstreamEntity> streamList = new ArrayList<>();
        List<String> recIds = new ArrayList<>();
        List<String> userList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            ActivityuserstreamEntity activityuserstreamEntity = activityUserToActivityUserstream(list.get(i));
            activityuserstreamEntity.setOpName(activityEntity.getApplyUserName());
            activityuserstreamEntity.setOpUser(activityEntity.getApplyUserId());
            userList.add(list.get(i).getUserId());
            streamList.add(activityuserstreamEntity);
            recIds.add(list.get(i).getRecId());
        }
        activityuserService.removeByIds(recIds);
        activityuserstreamService.saveBatch(streamList);
        //创建消息类对象
        MessageUtli messageUtli = new MessageUtli(activityEntity.getCreateOrganizationId(), activityEntity.getActivityName() + "已取消，请知悉", "活动删除通知", activityEntity.getCreateOrganizationName(), activityEntity.getActivityId(), DateUtils.getLocalDateTimeStr(),
                null, null, ConUtils.listToString(userList), null, activityEntity.getActivityName(), 0);
        sendMsg(messageUtli);
        return R.ok();
    }

    /**
     * 活动人员转活动人员流动
     *
     * @param activityuserEntity
     * @return
     */
    private ActivityuserstreamEntity activityUserToActivityUserstream(ActivityuserEntity activityuserEntity) {
        ActivityuserstreamEntity activityuserstreamEntity = new ActivityuserstreamEntity();
        activityuserstreamEntity.setActivityName(activityuserEntity.getActivityName());
        activityuserstreamEntity.setActivityId(activityuserEntity.getActivityId());
        activityuserstreamEntity.setUserName(activityuserEntity.getUserName());
        activityuserstreamEntity.setUserId(activityuserEntity.getUserId());
        activityuserstreamEntity.setOpTime(DateUtils.getLocalDateTimeStr());
        activityuserstreamEntity.setDealState(2);
        activityuserstreamEntity.setApplyType(2);
        return activityuserstreamEntity;
    }

    /**
     * 活动热度统计
     */
    @RequestMapping("/hoteNum/{activityId}")
    public R updateHoteNum(@PathVariable("activityId") String activityId) {
        String redisKey = Constant.COMMUNITY_ACTIVITY_HOT + activityId;
        long num = redisUtil.incr(redisKey, 1);
        if (num % 5 == 0) {
            ActivityEntity activityEntity = getActivityInfo(activityId);
            activityEntity.setHotNum((int) num);
            activityService.updateById(activityEntity);
            redisUtil.del(Constant.COMMUNITY_ACTIVITY + activityId);
            getActivityInfo(activityId);
        }
        return R.ok().put("activityNum", num);
    }

    /**
     * 根据活动ID查询此社团相关消息
     */
    @RequestMapping("/info/{activityId}")
    public R getCommunityByCommunityId(@PathVariable("activityId") String activityId) {
        return R.ok().put("data", getActivityInfo(activityId));
    }

    public ActivityEntity getActivityInfo(String activityId) {
        //加的为正常的锁非读写锁
//        RLock lock = redissonClient.getLock("communityInfoLock");
        //添加写锁  防止缓存出现脏数据 防止缓存穿透
        RReadWriteLock readLock = redisson.getReadWriteLock("activityInfoLock");
        RLock rLock = readLock.readLock();
        //设置 加锁时常   不设置默认为 看门狗  自动监控线程  30s解锁    设置时间要大于程序运行时间，不然出错
        rLock.lock();
        String key = Constant.COMMUNITY_ACTIVITY + activityId;
        if (redisUtil.hasKey(key)) {
            ActivityEntity activityEntity = JSON.parseObject(String.valueOf(redisUtil.get(key)), ActivityEntity.class);
            rLock.unlock();
            return activityEntity;
        } else {
            QueryWrapper<ActivityEntity> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("ACTIVITY_ID", activityId);
            ActivityEntity activityEntity = activityService.getOne(queryWrapper);
            if (org.springframework.util.StringUtils.isEmpty(activityEntity)) {
                //防止缓存穿透
                redisUtil.set(key, "null", 5);
                rLock.unlock();
                return null;
            }
            String s = JSON.toJSONString(activityEntity);
            redisUtil.set(key, s, 60 * 60 * 24 * 30 * 12 * 4);
            rLock.unlock();
            return activityEntity;
        }

    }
}
