package com.hww.communitymanage.activity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 15:53:07
 */
@Data
@TableName("act_activityuserstream")
public class ActivityuserstreamEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.ASSIGN_UUID)   //使用UUID策略来生成主键
	private String recId;
	/**
	 * 活动id
	 */
	private String activityId;
	/**
	 * 活动名称
	 */
	private String activityName;
	/**
	 * 人员id
	 */
	private String userId;
	/**
	 * 人员姓名
	 */
	private String userName;
	/**
	 * 管理员操作时间
	 */
	private String opTime;
	/**
	 * 申请人意见
	 */
	private String applyAdvice;
	/**
	 * 操作人id
	 */
	private String opUser;
	/**
	 * 操作人姓名
	 */
	private String opName;
	/**
	 * 审批状态0拒绝1通过
	 */
	private Integer dealState;
	/**
	 * 申请类别0退出活动1加活动2踢出活动3活动结束
	 */
	private Integer applyType;
	/**
	 * 最后更新时间
	 */
	private String updateTime;
	/**
	 * 拓展字段1
	 */
	private String applyTime;
	/**
	 * 拓展字段2
	 */
	@TableField(select = false)   //查询不返回此字段
	private String expand2;

}
