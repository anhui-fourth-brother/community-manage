package com.hww.communitymanage.message.dao;

import com.hww.communitymanage.message.entity.MessageEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 16:04:08
 */
@Mapper
public interface MessageDao extends BaseMapper<MessageEntity> {
	
}
