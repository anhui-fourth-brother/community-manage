package com.hww.communitymanage.message.rabbitmqConsumer;

import com.alibaba.fastjson.JSON;
import com.hww.common.utils.ConUtils;
import com.hww.common.utils.Constant;
import com.hww.common.utils.IdUtils;
import com.hww.common.utils.MessageUtli;
import com.hww.communitymanage.message.config.RedisUtil;
import com.hww.communitymanage.message.entity.AlertsEntity;
import com.hww.communitymanage.message.feign.UserListFeignService;
import com.hww.communitymanage.message.service.AlertsService;
import io.jsonwebtoken.lang.Strings;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@RabbitListener(queuesToDeclare = @Queue(value = "baseMsg"))
public class MsgConsumer {
    @Autowired
    AlertsService alertsService;
    @Autowired
    RedisUtil redisUtil;
    @Autowired
    UserListFeignService userListFeignService;
    @RabbitHandler
    public void reviceMsg(String message) {
        //消息转为实体类
        MessageUtli messageUtli = JSON.parseObject(message, MessageUtli.class);
        //库里存入该单消息
            List<String> acceptUsers = ConUtils.stringToList(messageUtli.getAcceptUserId());
            List<AlertsEntity> alertsEntityList = new ArrayList<>();
            for (int i = 0; i < acceptUsers.size(); i++) {
                AlertsEntity alertsEntity = messageToAlerts(messageUtli);
                alertsEntity.setAcceptUserId(acceptUsers.get(i));
                alertsEntityList.add(alertsEntity);
                redisUtil.incr(Constant.NO_READ_MESSAGE + alertsEntity.getAcceptUserId(), 1);
                redisUtil.incr(Constant.NEW_MESSAGE + alertsEntity.getAcceptUserId(), 1);
            }
            alertsService.saveBatch(alertsEntityList);
    }

    private AlertsEntity messageToAlerts(MessageUtli messageUtli) {
        AlertsEntity alertInfo = new AlertsEntity();
        alertInfo.setAcvitityId(messageUtli.getAcvitityId());
        alertInfo.setAcvitityName(messageUtli.getAcvitityName());
        alertInfo.setCommunityId(messageUtli.getCommunityId());
        alertInfo.setCommunityName(messageUtli.getCommunityName());
        alertInfo.setStateId(messageUtli.getStateId());
        alertInfo.setContent(messageUtli.getContent());
        alertInfo.setTitle(messageUtli.getTitle());
        alertInfo.setSendName(messageUtli.getSendName());
        alertInfo.setSendTime(messageUtli.getSendTime());
        alertInfo.setSendUserId(messageUtli.getSendUserId());
        return alertInfo;
    }
}
