package com.hww.communitymanage.message;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients(basePackages = "com.hww.communitymanage.message.feign")
public class CommunityMessageApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommunityMessageApplication.class, args);
    }

}
