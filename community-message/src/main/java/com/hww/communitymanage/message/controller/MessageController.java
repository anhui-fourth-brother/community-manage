package com.hww.communitymanage.message.controller;

import com.alibaba.fastjson.JSON;
import com.hww.common.utils.*;
import com.hww.communitymanage.message.config.RedisUtil;
import com.hww.communitymanage.message.entity.MessageEntity;
import com.hww.communitymanage.message.feign.UserListFeignService;
import com.hww.communitymanage.message.service.MessageService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;


/**
 * 
 *
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 16:04:08
 */
@RestController
@RequestMapping("message/message")
public class MessageController {
    @Autowired
    private MessageService messageService;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    UserListFeignService userListFeignService;
    @Autowired
    RedisUtil redisUtil;
    /**
     * 查询公告或者留言列表
     */
    private void sendMsg(Object msg) {
        String routingKey = "baseMsg";
        String content = JSON.toJSONString(msg);
        rabbitTemplate.convertAndSend(routingKey, content);
    }
    @RequestMapping("/list")
    //@RequiresPermissions("message:message:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = messageService.queryPage(params);
        return R.ok().put("page", page);
    }
    //处理消息数
    @RequestMapping("/messageNum/{userId}")
    public R list(@PathVariable("userId") String userId) {
       String noReadKey= Constant.NO_READ_MESSAGE+userId;
       String newMessageKey= Constant.NEW_MESSAGE+userId;
        Integer noReadMessage = (Integer)redisUtil.get(noReadKey);
        Integer newMessage = (Integer)redisUtil.get(newMessageKey);
        if (Objects.equals(noReadMessage,null)){
            noReadMessage=0;
        }
        if (Objects.equals(newMessage,null)){
            newMessage=0;
        }
        return R.ok().put("noReadMessage",noReadMessage).put("newMessage",newMessage);
    }
    /**
     * 根据主键查询信息
     */
    @RequestMapping("/info/{recId}")
    //@RequiresPermissions("message:message:info")
    public R info(@PathVariable("recId") String recId){
		MessageEntity message = messageService.getById(recId);

        return R.ok().put("message", message);
    }

    /**
     * 发送全员公告
     */
    @RequestMapping("/save")
    //@RequiresPermissions("message:message:save")
    public R save(@RequestBody MessageEntity message,@RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN+token))){
            return R.error(Constant.TOKEN,"登录信息失效，请重新登陆");
        }
		messageService.save(message);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("message:message:update")
    public R update(@RequestBody MessageEntity message,@RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN+token))){
            return R.error(Constant.TOKEN,"登录信息失效，请重新登陆");
        }
		messageService.updateById(message);
        return R.ok();
    }

//    /**
//     * 删除
//     */
//    @RequestMapping("/delete")
//    //@RequiresPermissions("message:message:delete")
//    public R delete(@RequestBody String[] recIds){
//		messageService.removeByIds(Arrays.asList(recIds));
//
//        return R.ok();
//    }

}
