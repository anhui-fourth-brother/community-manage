package com.hww.communitymanage.message.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hww.common.utils.PageUtils;
import com.hww.communitymanage.message.entity.DataEntity;

import java.util.Map;

/**
 * 
 *
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 16:04:08
 */
public interface DataService extends IService<DataEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

