package com.hww.communitymanage.message.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hww.common.utils.PageUtils;
import com.hww.common.utils.Query;

import com.hww.communitymanage.message.dao.DataDao;
import com.hww.communitymanage.message.entity.DataEntity;
import com.hww.communitymanage.message.service.DataService;


@Service("dataService")
public class DataServiceImpl extends ServiceImpl<DataDao, DataEntity> implements DataService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DataEntity> page = this.page(
                new Query<DataEntity>().getPage(params),
                new QueryWrapper<DataEntity>()
        );

        return new PageUtils(page);
    }

}