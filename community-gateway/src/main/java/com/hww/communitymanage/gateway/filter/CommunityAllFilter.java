package com.hww.communitymanage.gateway.filter;

import com.hww.common.utils.Constant;
import com.hww.common.utils.R;
import com.hww.communitymanage.gateway.config.RedisUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Component
public class CommunityAllFilter implements GlobalFilter, Ordered {
    @Autowired
    RedisUtil redisUtil;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        String token = exchange.getRequest().getHeaders().get("token").get(0);
        if (StringUtils.isNotEmpty(token) && redisUtil.hasKey(Constant.LOGIN_TOKEN + token)) {
            redisUtil.expire(Constant.LOGIN_TOKEN + token, 60 * 60 * 1);
        }
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
