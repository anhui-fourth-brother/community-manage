package com.hww.communitymanage.rule.dao;

import com.hww.communitymanage.rule.entity.RuleEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 社团规则表
 *
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-27 02:30:09
 */
@Mapper
public interface RuleDao extends BaseMapper<RuleEntity> {

}
