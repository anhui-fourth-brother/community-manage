package com.hww.communitymanage.rule.service.impl;

import org.springframework.stereotype.Service;

import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hww.common.utils.PageUtils;
import com.hww.common.utils.Query;

import com.hww.communitymanage.rule.dao.SchoolDao;
import com.hww.communitymanage.rule.entity.SchoolEntity;
import com.hww.communitymanage.rule.service.SchoolService;


@Service("schoolService")
public class SchoolServiceImpl extends ServiceImpl<SchoolDao, SchoolEntity> implements SchoolService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SchoolEntity> page = this.page(
                new Query<SchoolEntity>().getPage(params),
                new QueryWrapper<SchoolEntity>()
        );

        return new PageUtils(page);
    }

}